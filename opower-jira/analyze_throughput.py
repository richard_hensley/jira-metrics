#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Analyze Throughput by Calculating Arrival Rate into a specific status

Through put analysis is used to figure out how many tickets are arriving at a particular state in the system.
The most common use is to figure out how many tickets are arriving at done for a week or month. This gives you
a reasonable planning idea. If you look at the last 5 weeks of data, there is a 90% chance that the next week
will be between the highest and lowest value. You can use this in a monte carlo simulation to project
a number of weeks in the future.

This analysis program is used to figure out the throughput given the following parameters.

  Ticket Type(s)
  Bucketing Scheme (weekly, monthly, quarterly)
  How many buckets back
  Column(s) (a.k.a. Statuses)
  Data File(s)
  
This program will print out the throughput for each column in each bucket. This script only includes tickets that
are of the specified type, and that had at least one of the columns specified.

If you want to figure out how many tickets are done per week for the past 12 weeks for a file use the following command.

python analyze_throughput.py -t"User Story" -tTask -tBug -t"Technical Debt" -mweekly -b5 -cClosed data/DS_data.txt

And you will get output something like. (There is a tab between the weekly and closed numbers.
This makes it easy to read or paste into Excel.)

weekly  closed
201350  8
201351  10
201352  0
201401  3
201402  8
201403  16
201404  10
201405  26
201406  27
201407  13
201408  21
201409  20
201410  16
'''
import sys
import os
from datetime import date
from collections import OrderedDict
from utils.standardargs import add_common_args, standardmain
from utils.utils import parse_ticket_file
import logging

LOG = logging.getLogger()

def addargs(parser):
    add_common_args(parser, ('-c', '-m', '-b', '-t'))

def processor(args):
    types = args.types
    buckets = args.bucketer.buckets(date.today(), args.back)
    buckets = buckets[:-1]
    report = OrderedDict()
    LOG.debug('buckets %s',str(buckets))
    columns = args.columns
    for b in buckets:
        e = OrderedDict()
        for c in columns:
            e[c] = 0
        report[b] = e
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        for t in tickets:
            if not t.issue_type in types:
                continue
            for c in columns:
                d = t.first_arrival_date(c)
                if d:
                    bucket = args.bucketer.bucket(d)
                    if bucket in report:
                        r = report[bucket]
                        r[c] += 1
                        LOG.debug('ticket %s bucket %s', str(t), str(bucket))

    headings = [args.mode]
    headings.extend([v.lower().replace(' ', '_') for v in columns])
    print '\t'.join(headings)
    for month, r in report.iteritems():
        out = [str(month)]
        for count in r.itervalues():
            out.append(str(count))
        print '\t'.join(out)
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
