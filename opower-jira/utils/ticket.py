import json_objects
import dates
import datetime
from dates import date_to_str, datetime_to_str
import sprint_map

def status_str(status):
    if status == "Dev Readiness":
        status = "Dev Ready"
    return status

def calc_status_set(tickets):
    statuses = set()
    for t in tickets:
        statuses.add(status_str(t.status))
        for tr in t.transitions:
            statuses.add(status_str(tr.fromString))
            statuses.add(status_str(tr.toString))
    return statuses

class Transition(object):    
    def __init__(self, fs, ts, start, end):
        self.fromString = fs
        self.toString = ts
        self.startDate = start
        self.endDate = end
        
    def __str__(self):
        return "%s,%s" %(self.fromString,self.toString)
    
class FixVersionChange(object):
    def __init__(self,name,when):
        self.name = name
        self.when = when
        
    @property
    def maps_to_sprint(self):
        return sprint_map.map_sprint(self.name)
    
    @property
    def map_start_date(self):
        return sprint_map.map_start_date(self.name)
    
    @property
    def map_due_date(self):
        return sprint_map.map_due_date(self.name)
    
    @property
    def map_planning_end(self):
        return sprint_map.map_planning_end(self.name)
    
    def __str__(self):
        return "%s,wh=%s,st=%s,du=%s" % ((str(self.name), 
                                            datetime_to_str(self.when), 
                                            date_to_str(self.map_start_date),
                                            date_to_str(self.map_due_date)))

           
class Ticket(json_objects.JsonClass):

    history_item_prop_map = {
        'field'     : (None, None, 'field'),
        'from'      : (None, None, 'from'),
        'fromString': (None, None, 'fromString'),
        'to'        : (None, None, 'to'),
        'toString'  : (None, None, 'toString'),
        '__str__'   : ('field', 'fromString', 'toString')
                             }
    history_prop_map = {
        'created'   : (dates.str_to_datetime, None, 'created'),
        'items'     : (json_objects.make_list_item_factory(json_objects.make_value_factory(history_item_prop_map)),
                       [],
                       'items')
        }
    
    link_issue_prop_map = {
            'key' : (None, None, 'key'),
            'status' : (None, None, 'fields', 'status', 'name'),
            'issue_type' : (None, None, 'fields', 'issuetype', 'name')
            }
    
    link_prop_map = {
        'type'      : (None, None, 'type', 'name'),
        'inward'    : (None, None, 'type', 'inward'),
        'outward'   : (None, None, 'type', 'outward'),
        'inward_issue' : (json_objects.make_value_factory(link_issue_prop_map), {'key':None}, 'inwardIssue'),
        'outward_issue' : (json_objects.make_value_factory(link_issue_prop_map), {'key':None}, 'outwardIssue'),
                     }

    fix_version_prop_map = {
        'name'      : (None, None, 'name'),
        'release_date' : (dates.str_to_date, json_objects.DEFAULT_NONE, 'releaseDate'),
        'map_due_date' : (sprint_map.map_due_date, json_objects.DEFAULT_NONE, 'name'),
        'map_start_date' : (sprint_map.map_start_date,json_objects.DEFAULT_NONE, 'name'),
        '__str__'   : ('name', )
                            }    
    prop_map = {
        # get the ticket key, no default, no transformation
        'key'       : (None, None, 'key', ),
        # get the project from fields.project.key no transformation, no default
        'project'   : (None, None, 'fields', 'project', 'key'),
        # get the status from fields.status.name using status_str to parse, no default
        'status'    : (status_str, None, 'fields', 'status', 'name'),
        'issue_type' : (None, None, 'fields', 'issuetype', 'name'),
        'created'   : (dates.str_to_datetime, None, 'fields', 'created'),
        'updated'   : (dates.str_to_datetime, None, 'fields', 'updated'),
        'resolved'  : (dates.str_to_datetime, None, 'fields', 'resolutiondate'),
        'resolution' : (None, None, 'fields', 'resolution', 'name'),
        # get the scrum team from fields.customfield_11167.value with None as the default
        'scrum_team' : (None, json_objects.DEFAULT_NONE, 'fields', 'customfield_11167', 'value'),
        # get the start version, make a JsonClass with the fix_version_prop name, 
        # "name = None" is the default, 
        # from fields.customfield_10451
        'start_version' : (json_objects.make_value_factory(fix_version_prop_map), 
                           { 'name' : None }, 'fields', 'customfield_10451' ),
        'commitment_due_date' : (dates.str_to_datetime, None, 'fields', 'customfield_10174'),
        'fix_versions' : (json_objects.make_list_item_factory(json_objects.make_value_factory(fix_version_prop_map)),
                          [], 'fields', 'fixVersions'),
        'histories' : (json_objects.make_list_item_factory(json_objects.make_value_factory(history_prop_map)), 
                       [], 'changelog', 'histories'),
        'links' : (json_objects.make_list_item_factory(json_objects.make_value_factory(link_prop_map)), 
                       [], 'fields', 'issuelinks'),
        }

    def __init__(self, json):
        json_objects.JsonClass.__init__(self, json, Ticket.prop_map)

    @property
    def sortable_key(self):
        l = self.key.split('-')
        return int(l[1])
    
    def lead_time(self, from_statuses, to_status):
        from_date = []
        for s in from_statuses:
            from_date.append(self.first_arrival_date(s))
        to_date = self.first_arrival_date(to_status)
        return dates.calc_days_between(from_date, to_date)
    
    # Given a specific date, what is the status
    # When None is returned, the ticket did not exist on that date
    def status_on_date(self, when):
        # before ticket created, no status
        if when < self.created.date():
            return None
        # look for a status from the past
        for t in self.transitions:
            if when >= t.startDate.date() and when < t.endDate.date():
                return t.fromString
        # if not found in the past, must be current status
        return self.status
    
    # When did the ticket arrive in the status the last time
    def arrival_date(self, status, first=False):
        status = status_str(status)
        if status == self.status and len(self.transitions) == 0:
            return self.created.date()
        arrival = None
        for t in self.transitions:
            if t.toString == status:
                arrival = t.endDate.date()
                if first:
                    return arrival
        return arrival
    
    def first_arrival_date(self,status):
        return self.arrival_date(status, True)    
    
    # When did the ticket leave the status the last time
    def departure_date(self, status):
        status = status_str(status)
        if status == self.status:
            return None
        departure = None
        for t in self.transitions:
            if t.fromString == status:
                departure = t.startDate.date()
        return departure
    
    def has_all_statuses(self, statuses):
        for s in statuses:
            if not self.was_in_status(s):
                return False
        return True
    
    # How many days did the ticket spend in a specific status
    # Inclusive of all times the ticket was in the status
    def days_in_status(self, status):
        status = status_str(status)
        seconds = 0
        for t in self.transitions:
            if t.fromString == status:
                d = t.endDate - t.startDate
                seconds += d.total_seconds()
        if status == self.status:
            # find the start date
            now = datetime.datetime.now()
            if len(self.transitions) > 0:
                d = now - t.endDate
            else:
                d = now - self.created
            seconds += d.total_seconds()
        days = seconds / (24 * 60 * 60 * 1.0)
        if days < 0.1:
            return 0.0               
        return days

    def was_in_status(self, status):
        status = status_str(status)
        if self.status == status:
            return True
        for t in self.transitions:
            if t.fromString == status:
                return True
        return False
    
    @staticmethod
    def standard_field_header():
        return ['project','key','status','issue_type','resolution']
    
    @property 
    def standard_fields(self):
        return  [self.project, self.key, self.status, self.issue_type, str(self.resolution)]

    @property
    def fix_version_changes(self):
        try:
            return self._fix_version_changes
        except:
            self._fix_version_changes = []
            try:
                fv = self.fix_versions[0].name
            except IndexError:
                fv = None
            for history in self.histories:
                for item in history.items:
                    if item.field != 'Fix Version':
                        continue
                    if item.toString:
                        self._fix_version_changes.append(FixVersionChange(item.toString,history.created))
                        # the current fix version was found in the changes, so it was not set during creation
                        if fv == item.toString:
                            fv = None
            # take into account that the ticket could be created with a fix version
            if fv:
                self._fix_version_changes.insert(0,FixVersionChange(fv,self.created))
            return self._fix_version_changes

    @property
    def fix_version_transitions(self):
        result = []
        for fv in self.fix_version_changes:
            result.append("(%s)"%(str(fv),))
        return result
    
    @property
    def transitions(self):
        try:
            return self._transitions
        except:
            self._transitions = []
            last = self.created
            last = self.created
            for history in self.histories:
                for item in history.items:
                    if item.field != 'status':
                        continue
                    self._transitions.append(Transition(status_str(item.fromString), status_str(item.toString), 
                                                        last, history.created))
                    last = history.created
            return self._transitions
        
    @property
    def transition_states(self):
        last = 'Open'
        result = ['Open']
        for t in self.transitions:
            if t.toString != last:
                last = t.toString
                result.append(last)
        return result            
          
    @property
    def general_delivery_date(self):
        d = self.first_arrival_date('Closed')
        if not d:
            d = self.first_arrival_date('Done')
        return d
    
    @property
    def first_fix_version_name(self):
        for fv in self.fix_versions:
            return fv.name
        return None

    @property
    def first_fix_version_release_date(self):
        for fv in self.fix_versions:
            return fv.release_date
        return None
    
    @property
    def is_initiative(self):
        return self.issue_type == 'Product Initiative'
    
    @property
    def is_feature(self):
        if 'Pre V3.31.x' in [fv.name for fv in self.fix_versions]:
            return False
        return self.issue_type == 'Feature'
    
    @property
    def is_work(self):
        return self.issue_type in ('User Story', 'Bug', 'Task', 'Technical Debt')
    
    @property
    def is_closed(self):
        return self.status in ('Closed', 'Done')
        
    @staticmethod
    def parse_team_file(team_project_file):
        with open(team_project_file) as f:
            Ticket.team_projects = [v.strip().upper() for v in f if v.strip()]
        
    def __str__(self):
        return self.key

    excluded_versions = set(('Pre V3.31.x','Pre V3.34.x','V3.35.x','V3.34.x','V3.33.x',
                           'V3.32.x','V3.31.x','V3.30.x'))
    def old_stuff(self):
        if self.issue_type != 'Product Initiative':
            if self.start_version.name in Ticket.excluded_versions:
                return True
            try:
                if self.fix_versions[0].name in Ticket.excluded_versions:
                    return True
            except:
                pass
        return False
    
    
