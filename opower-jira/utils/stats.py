import math


def mean(data):
    if data:
        return sum(data) / (1.0 * len(data))
    return 0.0

def stddev(data, pmean = None):
    if pmean:
        m = pmean
    else:
        m = mean(data)  
    if data:
        sq = [pow(x-m,2) for x in data]
        return pow(mean(sq), 0.5), m
    return 0.0, m

def zscore(data, psd = None, pmean = None):
    if psd and pmean:
        sd = psd
        m = pmean
    else:
        sd, m = stddev(data)
    if data:
        return [0 if not sd else (x - m)/sd for x in data], sd, m
    return 0.0, sd, m

def percentile(N, percent, key=lambda x:x):
    """
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return 0.0
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c-k)
    d1 = key(N[int(c)]) * (k-f)
    return d0+d1