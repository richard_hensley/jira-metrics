import sys

DEFAULT_NONE = object()

def json_get(json, path):
    d = json
    for p in path:
        if isinstance(d,dict):
            # this might blow a KeyError, and that's ok. 
            # It indicates the mapping is wrong
            d = d[p]
        elif isinstance(d,(list,tuple)):
            # this might blow a conversion error, that's ok.
            p = int(p)
            d = d[p]
    return d

#use this method for simple prop maps to json stuff
def make_value_factory(prop_map):
    return (lambda json:JsonClass(json, prop_map))

class JsonClass(object):
    '''
    The purpose of this class is to map property names to json paths.
    
    This class works by overriding the __getattr__ method. It looks the property name up in the
    map. If it exists, it navigates the json, and stores the result value in the __dict__ instance
    attribute dictionary so that all future values are accessed quickly.
    '''
    def __init__(self, json, prop_map):
        self._prop_map = prop_map
        self._json = json
        
    def get(self, path):
        return json_get(self._json, path)
    
    def __str__(self):
        try:
            config = self._prop_map['__str__']
            values = []
            for attr in config:
                values.append(self.__getattr__(attr))
            return ','.join(map(str,values))
        except:
            return str(self._json)
    
    def __getattr__(self, name):
        # this is only called the first time a property is referenced
        # because it pokes the value directly into the instance dictionary
        try:
            config = self._prop_map[name]
        except:
            raise AttributeError('property not found in map %(name)s' % locals())
        # navigate the json
        try:
            value = self.get(config[2:])
        except:
            if config[1]:
                if config[1] == DEFAULT_NONE:
                    value = None
                else:
                    value = config[1]
            else:
                raise AttributeError('invalid json path for property %s\n%s' % (name,sys.exc_info()[0]))
        # use the factory to cast the value to the proper type
        try:
            if config[0]:
                value = config[0](value)
        except:
            raise AttributeError('invalid factory function for property %s\n%s' % (name, sys.exc_info()[0]))
        # assign the value to the instance dictionary to speed up access next time
        setattr(self, name, value)
        return value

def make_list_item_factory(json_value_factory):
    return (lambda json_list: JsonList(json_list,json_value_factory))
            
class JsonList(object):
    def __init__(self, json_list, json_value_factory):
        self.json_list = json_list
        self.json_value_factory = json_value_factory
    
    def make(self, value):
        return self.json_value_factory(value)
        
    def __iter__(self):
        return iter(map(self.make,self.json_list))
    
    def __getitem__(self, key):
        return self.make(self.json_list[key])
    
    def __len__(self):
        return len(self.json_list)
    
    def __str__(self):
        return str(self.json_list)
    