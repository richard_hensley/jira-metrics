#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Print out initiative tickets

'''
import sys
import os
from datetime import date, timedelta
from utils.standardargs import standardmain, add_common_args
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.dates import date_to_str
from utils.ticket import Ticket
import logging

LOG = logging.getLogger()

def addargs(parser):
    add_common_args(parser, ('-t', '-b',))

def do_filter(w, when, types):
    if w and w.is_closed and w.delivery_date >= when and w.ticket.issue_type in types:
        return True
    return False

def processor(args):
    when = date.today() - timedelta(days=args.back)
    LOG.debug('when %s',when.isoformat())
    types = set(args.types)
    
    headings = Ticket.standard_field_header()
    headings.extend((
                'due_date', 
                'start_date',
                'delivery_date', 
                'delivery_delta', 
                'lead_time', 
                'variance'))
    print '\t'.join(headings)
    data = []
    for fn in args.infile:
        tickets = filter(lambda w: do_filter(w, when, types), 
            map(ticket_wrapper_factory, parse_ticket_file(fn)))
        for w in tickets:
            out = w.ticket.standard_fields
            out.extend((
               date_to_str(w.due_date),
               date_to_str(w.start_date),
               date_to_str(w.delivery_date),
               w.delivery_delta if w.delivery_delta else '',
               w.lead_time if w.lead_time else '',
               w.delivery_variance if w.delivery_variance else ''))
            data.append(
                (out,
                 w.delivery_date,
                 w.ticket.sortable_key))

    data.sort(key=lambda x: (x[1], x[2]))

    for d in data:
        print '\t'.join(map(str,d[0]))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
