#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Print out the jira keys for all the tickets in the specified file.

This is a diagnostic utility used when trying to figure out what jira tickets are contained in a file.
'''
import sys
import os
from utils.utils import parse_ticket_file
from utils.standardargs import standardmain


def addargs(parser):
    pass


def processor(args):
    print 'key'
    for fn in args.infile:
        for t in parse_ticket_file(fn):
            print t.key
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
