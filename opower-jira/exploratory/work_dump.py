#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Diagnostic dump of Task, User Story, Technical Debt, and Bug
'''
import sys
import os
from utils.standardargs import standardmain
from utils.ticket import Ticket
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.dates import date_to_str


def addargs(parser):
    pass


def processor(args):
    headings = Ticket.standard_field_header()
    headings.extend(('start_date', 'due_date', 'delivery_date', ))
    print '\t'.join(headings)
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        for w in map(ticket_wrapper_factory, filter(lambda x: x.is_work and x.is_closed, tickets)):
            out = w.ticket.standard_fields
            out.extend((
                date_to_str(w.start_date),
                date_to_str(w.due_date),
                date_to_str(w.delivery_date),
                '' if w.start_date else ','.join(w.ticket.transition_states)
            ))
            print '\t'.join(out)
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
