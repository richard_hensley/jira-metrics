#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Use the -f parameter and summarize with counts all values occuring in that field from the selected tickets.

This is used to get a sense of the status values used in the data set.
'''

import sys
import os
from collections import OrderedDict
from utils.standardargs import add_common_args, standardmain
from utils.utils import parse_ticket_file


def inc(d, s):
    if s not in d:
        d[s] = 0
    d[s] += 1


def addargs(parser):
    add_common_args(parser, ('-t',))
    parser.add_argument(
        '-f', '--field', dest='fields', action='append', required=True)


def processor(args):
    fields = args.fields
    if args.types:
        types = args.types
    else:
        types = None

    headings = ['value', 'count']
    print '\t'.join(headings)
    counts = OrderedDict()
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        for t in tickets:
            if types and t.issue_type not in types:
                continue
            inc(counts, t.get(fields))

    for (v, count) in counts.iteritems():
        print '\t'.join([str(v) for v in [v, count]])

    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
