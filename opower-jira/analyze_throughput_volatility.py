#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Analyze Throughput Volatility by aggregating data into buckets and normalizing across a long term time frame.

This analysis program is used to figure out the throughput given the following parameters.

  Ticket Type(s)
  Bucketing Scheme (weekly, monthly, quarterly)
  How many buckets back
  Data File(s)
'''
from datetime import date
from collections import OrderedDict
from utils.standardargs import add_common_args, standardmain
from utils.utils import parse_ticket_file, ticket_wrapper_factory
import sys
import os
from utils.stats import stddev, zscore
import logging

LOG = logging.getLogger()

def addargs(parser):
    add_common_args(parser, ('-m', '-b', '-t'))
    parser.add_argument('-r', '--red', dest='red', type=float, default=1.1)
    parser.add_argument('-g', '--green', dest='green', type=float, default=0.6)

def processor(args):
    types = args.types
    buckets = args.bucketer.buckets(date.today(), args.back)
    buckets = buckets[:-1]
    LOG.debug('buckets %s',str(buckets))
    report = OrderedDict()
    for b in buckets:
        report[b] = 0
    total = 0
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        for w in filter(None, map(ticket_wrapper_factory, tickets)):
            if not w.ticket.issue_type in types:
                continue
            if not w.ticket.is_closed:
                continue
            bucket = args.bucketer.bucket(w.delivery_date)
            try:
                report[bucket] += 1
                total += 1
                LOG.debug('ticket %s bucket %s', str(w), str(bucket))
            except:
                pass

    sd, m = stddev(report.values())
    print '\t'.join(map(str, ['mean', m, 'file', args.infile]))
    print '\t'.join(map(str, ['sd', sd, 'type', args.types]))
    print '\t'.join(map(str, ['red', args.red, 'mode', args.mode]))
    print '\t'.join(map(str, ['green', args.green]))
    headings = [args.mode, 'volatility',
                'throughput', 'red', 'yellow', 'green']
    print '\t'.join(headings)
    for bucket, r in report.iteritems():
        z = abs(zscore((r,), sd, m)[0][0])
        out = [bucket, z, r if r > 0 else '',
               z if z >= args.red else '',
               z if z < args.red and z >= args.green else '',
               z if z < args.green else '']
        print '\t'.join(map(str, out))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
