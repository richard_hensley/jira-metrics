'''
Created on Mar 14, 2014

@author: Richard
'''
import unittest

from datetime import date
from utils.buckets import DailyBucketer, WeeklyBucketer, MonthlyBucketer,\
    QuarterlyBucketer, SprintsBucketer
from utils import sprint_map
from utils.utils import ONEDAY

class Test(unittest.TestCase):
    
    def setUp(self):
        load_mappings()

    def test_calc_day_back(self):
        self.assertEquals(DailyBucketer().back(date(2014,2,5),3),date(2014,2,2))
    
    def test_calc_week_start(self):
        self.assertEquals(WeeklyBucketer().start(date(2014,2,4)),date(2014,2,2))
        
    def test_calc_week_back(self):
        self.assertEquals(WeeklyBucketer().back(date(2014,2,4),3), date(2014,1,12))
        
    def test_calc_week_back2(self):
        self.assertEqual(WeeklyBucketer().back(date(2014,1,24),6),date(2013,12,8))
        
    def test_calc_month_back(self):
        self.assertEquals(MonthlyBucketer().back(date(2014,2,4),2), date(2013,12,1))
        
    def test_calc_quarter_begin(self):
        self.assertEquals(QuarterlyBucketer().start(date(2014,2,5)),date(2014,1,1))
        
    def test_calc_quarter_back(self):
        self.assertEquals(QuarterlyBucketer().back(date(2014,2,4),2), date(2013,7,1))

    def test_calc_quarter(self):
        q = QuarterlyBucketer().bucket(date(2014,1,25))
        self.assertEquals('2014Q1',q)
        q = QuarterlyBucketer().bucket(date(2014,2,25))
        self.assertEquals('2014Q1',q)

    def test_calc_month(self):
        q = MonthlyBucketer().bucket(date(2014,1,25))
        self.assertEquals('2014M01',q)
        q = MonthlyBucketer().bucket(date(2014,2,25))
        self.assertEquals('2014M02',q)
        
    def test_calc_week(self):
        q = WeeklyBucketer().bucket(date(2014,1,25))
        self.assertEquals('2014W04 V4.2.x',q)
        q = WeeklyBucketer().bucket(date(2014,2,25))
        self.assertEquals('2014W09 V4.3.x',q)
        self.assertEquals('2013W52 V3.50.x',WeeklyBucketer().bucket(date(2013,12,29)))
        self.assertEquals('2014W01 V4.1.x',WeeklyBucketer().bucket(date(2013,12,30)))
        self.assertEquals('2014W01 V4.1.x',WeeklyBucketer().bucket(date(2014,1,1)))

    def test_quarter_buckets(self):
        b = QuarterlyBucketer().buckets(date(2014,1,25),2)
        self.assertEquals(['2013Q3','2013Q4','2014Q1'],b)
        
    def test_month_buckets(self):
        b = MonthlyBucketer().buckets(date(2014,1,25), 3)
        self.assertEquals(['2013M10','2013M11','2013M12','2014M01'], b)
        
    def test_week_buckets(self):
        b = WeeklyBucketer().buckets(date(2014,1,24), 6)
        self.assertEquals(['2013W49 V3.49.x','2013W50 V3.50.x','2013W51 V3.50.x',
                           '2013W52 V3.50.x','2014W01 V4.1.x','2014W02 V4.1.x',
                           '2014W03 V4.1.x','2014W04 V4.2.x'],b)
                    
    def test_sprint_buckets_v42x(self):
        load_mappings()
        b = SprintsBucketer()
        self.assertEquals(b.bucket(date(2014,1,20)),'V4.2.x 2014Q1')
        self.assertEquals(b.bucket(date(2014,1,24)),'V4.2.x 2014Q1')
        self.assertEquals(b.bucket(date(2014,2,9)),'V4.2.x 2014Q1')
                
    def test_sprint_buckets_v41x(self):
        load_mappings()
        b = SprintsBucketer()
        self.assertEquals(b.bucket(date(2014,1,2)),'V4.1.x 2014Q1')
        self.assertEquals(b.bucket(date(2014,1,3)),'V4.1.x 2014Q1')
        self.assertEquals(b.bucket(date(2014,1,19)),'V4.1.x 2014Q1')
        
    def test_sprint_buckets_Q1Q2(self):
        b = SprintsBucketer()
        start = date(2013,12,20)
        end = date(2014,4,3)
        buckets = set()
        while start <= end:
            bucket = b.bucket(start)
            buckets.add(bucket)
            start += ONEDAY
        self.assertSetEqual(buckets, set(('V4.5.x 2014Q2', 'V4.2.x 2014Q1', 
                                          'V4.4.x 2014Q1', 'V4.1.x 2014Q1', 
                                          'V4.3.x 2014Q1', 'V3.50.x 2013Q4')))
        
    def test_sprint_bucket_fail(self):
        load_mappings()
        b = SprintsBucketer()
        self.assertIsNone(b.bucket(date(2012,1,1)))

    def test_sprint_start_v41x(self):
        load_mappings()
        b = SprintsBucketer()
        self.assertEquals(b.start(date(2014,1,2)),date(2013,12,30))
        self.assertEquals(b.start(date(2014,1,3)),date(2013,12,30))
        self.assertEquals(b.start(date(2014,1,19)),date(2013,12,30))
        
    def test_sprint_back(self):
        load_mappings()
        b = SprintsBucketer()
        self.assertEquals(b.back(date(2014,3,12),2),date(2014,1,20))
        
    def test_sprint_back_fail(self):
        load_mappings()
        b = SprintsBucketer()
        self.assertIsNone(b.back(date(2014,1,2),6))
        
    def test_sprint_none(self):
        load_mappings()
        b = SprintsBucketer()
        self.assertIsNone(b.bucket(None))
 
    
def stub_load_mappings():
    pass

def do_load_mappings():
    try:
        sprint_map.SprintMap.parse_sprint_map('./sprint_mappings.txt','./sprint_alias_mapping.txt')
    except IOError:
        sprint_map.SprintMap.parse_sprint_map('./test/sprint_mappings.txt','./test/sprint_alias_mapping.txt')
    load_mappings = stub_load_mappings

load_mappings = do_load_mappings


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()