'''
Created on Mar 20, 2014

@author: Richard
'''
import unittest
from xl.worksheet import Worksheet
from xl.cell import Cell
from xl.util import to_xml
import datetime

class Parm(object):
    def __init__(self):
        self.date_style = 1
        
class Test(unittest.TestCase):

    def test_cell_int(self):
        c = Cell('A1')
        c.value = 5
        self.assertEquals('<c r="A1"><v>5.0</v></c>',to_xml(c,None))

    def test_cell_float(self):
        c = Cell('A1')
        c.value = 3.1
        self.assertEquals('<c r="A1"><v>3.1</v></c>',to_xml(c, None))

    def test_cell_string(self):
        c = Cell('A1')
        c.value = 'test'
        self.assertEquals('<c t=\"inlineStr\" r="A1"><is><t>test</t></is></c>',to_xml(c,None))
        
    def test_cell_date(self):
        c = Cell('A1')
        c.value = datetime.date(2014,1,2)
        self.assertEquals('<c s="1" t="d" r="A1"><v>2014-01-02</v></c>',to_xml(c, Parm()))
            
    def test_cell_none(self):
        c = Cell('A1')
        self.assertEquals('',to_xml(c,None))

    def test_sheet(self):
        ws = Worksheet()
        ws.cell(0,0).value=5
        ws.cell(0,1).value='Test'
        ws.cell(1,0).value = None
        self.assertEquals(to_xml(ws,None),'<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"><sheetData><row r="1"><c r="A1"><v>5.0</v></c><c t="inlineStr" r="B1"><is><t>Test</t></is></c></row><row r="2"></row></sheetData></worksheet>')

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_workseet']
    unittest.main()