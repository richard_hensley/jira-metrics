'''
Created on Mar 17, 2014

@author: Richard
'''
import unittest
from utils.stats import mean, stddev, zscore


class Test(unittest.TestCase):

    data = (2,4,4,4,5,5,7,9)

    def test_mean(self):
        self.assertEquals(5.0,mean(Test.data))

    def test_sd(self):
        self.assertEquals(2.0,stddev(Test.data)[0])
        
    def test_z(self):
        self.assertEquals([-1.5,-0.5,-0.5,-0.5,0.0,0.0,1.0,2.0],zscore(Test.data)[0])

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_mean']
    unittest.main()