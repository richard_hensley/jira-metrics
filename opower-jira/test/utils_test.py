
import unittest
from utils.utils import first, ticket_wrapper_factory, WorkWrapper
from utils import ticket
import json

class Test(unittest.TestCase):
            
    def load_ticket(self, fn):
        try:
            with open(fn) as f:
                j = json.load(f)
        except IOError:
            with open('test/'+fn) as f:
                j = json.load(f)
        return ticket.Ticket(j) 
    
    def load_wrapper(self, fn):
        ticket = self.load_ticket(fn)
        return ticket_wrapper_factory(ticket)
    
    def test_first(self):
        self.assertEquals(1,first([None, 1, 2, None]))
    
    def test_wrapper_factory(self):
        w = self.load_wrapper('test_ticket_ds_1609.json')
        self.assertIsInstance(w, WorkWrapper)
        
    def test_lead_time(self):
        w = self.load_wrapper('test_ticket_ds_692.json')
        self.assertIsNotNone(w.start_date)
        self.assertIsNotNone(w.delivery_date)
        self.assertEquals(w.lead_time,11.0)  

    def test_due_date(self):
        w = self.load_wrapper('test_ticket_rm_216.json')
        self.assertIsNotNone(w.due_date)
        self.assertIsNotNone(w.delivery_date)
        self.assertIsNotNone(w.start_date)
        self.assertEquals(w.delivery_delta,-112.0)
        self.assertEquals(w.lead_time,253.0)
        self.assertAlmostEqual(w.delivery_variance,-0.44,2)
                      
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testUtils']
    unittest.main()