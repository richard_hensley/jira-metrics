# Jira Metrics Support Applications

This set of python scripts are designed to transform the ticket data in the 
Opower jira server into something consumable by excel. The goal is to create 
a dashboard of metrics useful for managing and monitoring the delivery of 
initiatives, features, and work.

# Data Notes

Initiative
 * The fix version in the RM project uses the release date field, so set it 
 when adding a new release version.
 * Type is "Product Initiative"
 * Due Date is calculated using the fix version due date. When that does not 
 exist, the commitment due date is used.
 
Feature
 * The start date is very unreliable for data before June 2014. The new feature
 work system started up around April 2014 for a couple of teams, and with that
 move to new jira boards, the data became much cleaner.
 * Delivery Date is calculated based on the date the ticket is closed.
 * Features are WIP when they are in one of Release Backlog, Decomposition, 
 Dev Ready, Build, Demo, or Ready for Delivery on a specific date.
 * Due date is calculated differently for planned vs. unplanned work. Planned
 work is calculated by taking the release date from the last fix version set
 before the planning window closes. The planning window is set when the fix 
 version is first set. From that date until the beginning of the next quarter
 is the planning window. For unplanned work, the fix version is used to 
 determine due date. In that case, the due date is stored in the sprint mapping
 table.

Work

# How Tribes Are Handled!

The tribes are a collection of jira projects. As such, the data from
all the jira projects, and selected project data from the RM project
can be joined into a single data set to represent the tribal data set.

So, tribes are handled as big projects of Features, Initiatives, and Work Items. '
All the data transforms and spreadsheets work correctly once the tribal data 
sources are created.

the files script/make_tribe_* show how the join_tribal_data.py script is
used to create tribal data files.

# Design

This application suite works in two phases.

1. Download the data Jira ticket into locally stored text files. The files 
contain all tickets that qualify for a specific JQL. The locally stored files 
have a JSON header, and each ticket is a row in the file formatted with JSON. 
The program that does this is called data_dump.py. It has a variety of parameters 
to get it's job done. The program jira_download.py is a driver program that 
downloads all the data required for the current metrics program.

2. Process the ticket files into tab separated text text files easily consumable 
by Excel.

# Install

* Install python 2.7 according to instructions on python.org
* Download the repository
* Install the requirements listed in _requirements.txt_
* change into the root directory of the repository
* mkdir data
* mkdir output
* mkdir visualization

# Operations

ALL scripts must be executed from the room directory of the repository. The
file that contains do_scripts.py

* Do Everything in the Correct Order

```
python do_scripts.py -jconfig/jira_config.json scripts/suite_*
```

* Download the data
```
python do_scripts.py -jconfig/jira_config.json scripts/suite_download
```
    
* Refresh the aggregate calculations and visualizations

```
python do_scripts.py -jconfig/jira_config.json scripts/suite_initiative scripts/suite_tribe scripts/suite_team
```

* Refresh visualizations

```
python do_scripts.py -jconfig/jira_config.json scripts/visualize_all
```


* Review the spreadsheets in the visualization directory

# Frequent Updates That Might Need to be done

## Adding a jira field to the ticket

The jira tickets are stored as json form. The class JsonClass and JsonList define
the classes used to navigate around the jira json. The basic usage is to 
create a name, and a definition for how to find the data in the json. The 
following examples show the basic work. These definitions are present in the
JiraTicket class. Look at JiraTicket.prop_map for examples with comments.

## Updating the start date workflow steps for an Initiative, Feature, Work Item

The InitiativeWrapper, FeatureWrapper, and WorkWrapper classes have a set 
constance called WIP_STATUS which is the list of statuses correlating to
WIP. The start date is the earliest date the tickets enters any one of the
status values.

## Reordering the dates used for start date

The start_date and due_date use the date selector functions to figure out
the dates. The order of dates sent to the selector determines which date
will be used. See FeatureWrapper.due_date for an example

## Changing the definition of a work item

The definition of a work item is triggered by using the command line parameter
-tAllWork which translates to -tUser Story -tTask -tTechnical Debt -tBug. This 
is implemented in the process_standard_args function. Modify that to change
the list.

## Add or modify a tribe

1) Update the file scripts/make_tribal_join to make a tribal file, or to
update a currently created tribe
2) update the tribes.txt file to add the code to the file

* Config Directory Files
 
# all_projects.txt

This file is used by scripts/suite_download to determine which projects to
download data from jira.

# jira_config.json

The recommended file name for the private jira credential storage. See 
sample_jira_config.json for the contents.

# portfolios.txt

This is the list of portfolio projects. It is used to scripts/suite_portfolio
to determine which data sets to create and use in spreadsheet creation.

# sample_jira_config.json

This is an example jira credential configuration file. Copy this file, name 
it jira_config.json, and then use the file jira_config.json

# sprint_alias_mapping.txt

This file maps invalid sprint names, like 4.09.x to valid sprint names like 
4.9.x

# sprint_mappings.txt

This file maps sprints to due dates, start dates, quarters, and years. This
file is tab delimited, and has a specific ordering of columns. The best
way to edit this file is to open the file with a text editor, copy the 
contents into excel, make your changes, copy the data, and paste it back
into the text editor.

# teams.txt

This file is used by scripts/suite_team to determine which datasets to create
and which spreadsheets to create.

# tribe_mapping.txt

This file documents the tribe to team mappings. It is not used by any program.

# tribes.txt

This file is used by scripts/suite_tribe to determine which datasets to create 
and which spreadsheets to create.

* Directory Structure

# config

This contains all the configuration files.

# data

This directory must be created. The download scripts make all the raw data
files in this directory

# exploratory

This directory contains a bunch of exploratory and diagnostic scripts that
can be used to explory the data.

# output

This directory is where the aggregate datasets are created by the scripts.

# scripts

This directory contains the python program execution scripts with all their
parameters. A script can be executed using the do_script.py program.

# templates

This directory contains the Excel spreadsheet templates.

# test

This directory contains the unit tests for the tricky bits of python code.

# utils

This is the library of routines used in common by all the metrics scripts.

# visualization

This is the output directory where spreadsheets are deposited after the
transformation process.

# xl

This is the Excel transformation library.

* How to update spreadsheet templates

The spreadsheet templates are stored in the template directory. The templates
are updated with Excel, and there are a few rules to getting them right.

1) Put a date somewhere in the any sheet, and set the type to Date. This is 
because of the way Excel stores dates. The transform can not add the needed
elements to the spreadsheet for dates, so they have to exist.

2) Any sheet name contains a "_" is assumed to be part of a dataset file. 
This sheet name will be used with the project name appened to find a data set. 
If it can not, the program throws an exception.

3) Any sheet named according to the rules above is completely replaced 
with the contents of the data file as if the user did the following process.
Open the text file in a notepad, select all, copy, open the excel sheet, 
navigate to the sheet in question cell A1, paste the clipboard.

4) All formulas and graphs are recalculated when Excel opens the spreadsheet
so don't be shy.

5) Ensure that there is a file in the scripts/ directory that directs the
system to transform your spreadsheet template. See scripts/visualize_work_all
for an example