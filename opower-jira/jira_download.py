#!/usr/local/bin/python2.7
# encoding: utf-8
'''
This is the primary downloader script. This script is used to download and update a text file of jira
tickets in json format. The json files are used by the analysis and diagnostic scripts for their processing.

This program take the following parameters:
    Jira url
    Jira user
    Jira user password
    output file
    days back
'''
import sys
from utils.dates import str_to_date
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from datetime import date, timedelta
from utils.utils import parse_ticket_info, parse_ticket_file, save_ticket_file
from jira.client import JIRA
from utils.buckets import MonthlyBucketer
from utils.standardargs import process_config_args, add_logging_args,\
    process_logging_args


def main(argv=None):  # IGNORE:C0111

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_shortdesc = 'jira_download.py dumps jira data into a text file. It includes transitions, and basic data'
    try:
        # Setup argument parser
        parser = ArgumentParser(
            description=program_shortdesc,
            formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument(
            '-x', '--config', dest='configs', action='append', required=True)
        parser.add_argument("-o", "--out", dest="outfile", required=True)
        parser.add_argument(
            "-d", "--days_back", dest="days", type=int, required=True)
        parser.add_argument(dest="jql", nargs='+')
        add_logging_args(parser)

        # Process arguments
        args = parser.parse_args()
        process_config_args(args)
        process_logging_args(args)

        server = args.server
        jql = args.jql
        user = args.user
        password = args.password
        outfile = args.outfile

        # if it exists, get the current ticket info
        info = parse_ticket_info(outfile)
        info['jira_server'] = server
        info['jira_user'] = user
        info['output_file'] = outfile
        if 'data_pulled_date' not in info:
            print 'Making new file %s' % (outfile, )
            w = MonthlyBucketer.calc_month_start(
                date.today() - timedelta(days=args.days))
            info['data_pulled_date'] = w.isoformat()
        else:
            print 'Updating file %s' % (outfile, )
        info['days_back'] = args.days
        tickets = {}
        try:
            temp = parse_ticket_file(outfile)
            for t in temp:
                tickets[t.key] = t._json
        except IOError:
            pass

        jql = ' '.join(jql)
        jql = jql.replace('$WontFix$', '"Won\'t Fix"')
        pulled = str_to_date(info['data_pulled_date'])
        pulled -= timedelta(days=2)
        pulled = pulled.isoformat()
        jql = jql.replace('$UPDATED$', 'updated >= %s' % (pulled,))
        info['jql'] = jql
        info['data_pulled_date'] = date.today().isoformat()

        options = {
            'server': server,
        }
        jira = JIRA(options, basic_auth=(user, password))
        issues = jira.search_issues(jql, fields="summary", maxResults=1)
        total = issues.total
        info['updated_records'] = total

        print "Query Keys total=%d" % (total, )
        count = 0
        keys = []
        while count < total:
            issues = jira.search_issues(
                jql, fields="summary", startAt=count, maxResults=500)
            for issue in issues:
                keys.append(issue.key)
            count += 500
        count = 0
        print "Load Data total=%d" % (total, )
        for key in keys:
            retry = 0
            count += 1
            if count % 100 == 0:
                print "Process %d of %d" % (count, total)
            try:
                issue = jira.issue(key, expand="changelog")
                tickets[key] = issue.raw
            except Exception as e:
                retry += 1
                if retry > 3:
                    raise e
                print "Retry Key ", key
        print "Process %d of %d" % (count, total)
        print "Done"

        save_ticket_file(outfile, info, tickets.itervalues())
        return 0

    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0

if __name__ == "__main__":
    sys.exit(main())
