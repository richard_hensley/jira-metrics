
from util import start_tag, end_tag
import cell

class Worksheet(object):
    
    def __init__(self):
        self._cells = {}
        self.max_row = 0
        self.max_column = 0

    def cell(self, row, column):
        if row > self.max_row:
            self.max_row = row
        if column > self.max_column:
            self.max_column = column
        coordinate = '%s%s' % (get_column_letter(column + 1), row + 1)
        try:
            result = self._cells[coordinate]
        except KeyError:
            result = self._cells[coordinate] = cell.Cell(coordinate)
        return result
    
    def xml(self, doc, parms):
        start_tag(doc, 'worksheet',
                {
                'xmlns': 'http://schemas.openxmlformats.org/spreadsheetml/2006/main',
                })
        start_tag(doc,'sheetData')
        for row in range(self.max_row + 1):
            start_tag(doc,'row', { 'r' : row + 1 })
            for column in range(self.max_column + 1):
                self.cell(row, column).xml(doc, parms)
            end_tag(doc,'row')
        end_tag(doc, 'sheetData')
        end_tag(doc, 'worksheet')

def get_column_letter(col_idx):
    if not 1 <= col_idx <= 18278:
        msg = 'Column index out of bounds: %s' % col_idx
        raise ValueError(msg)
    letters = []
    while col_idx > 0:
        col_idx, remainder = divmod(col_idx, 26)
        # check for exact division and borrow if needed
        if remainder == 0:
            remainder = 26
            col_idx -= 1
        letters.append(chr(remainder+64))
    return ''.join(reversed(letters))
