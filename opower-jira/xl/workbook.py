from collections import OrderedDict

import util
import worksheet
from xl.ooxml import PACKAGE_WORKSHEETS, ARC_CONTENT_TYPES, WORK_OR_CHART_TYPE,\
    ARC_WORKBOOK, VALID_WORKSHEET, CONTYPES_NS, SHEET_MAIN_NS, ARC_STYLE
from xml.etree.ElementTree import fromstring, tostring

def clean_formula_values(data):
    root = fromstring(data)
    # clean out formula values
    n = '{%s}sheetData/{%s}row/{%s}c' % (SHEET_MAIN_NS,SHEET_MAIN_NS,SHEET_MAIN_NS)
    for c in root.findall(n):
        v = c.find('{%s}v' % (SHEET_MAIN_NS,))
        f = c.find('{%s}f' % (SHEET_MAIN_NS,))
        if f is not None and v is not None:
            c.remove(v)
    return tostring(root)

class WorkbookTemplate(object):

    def __init__(self, template, worksheet_names, date_style):
        self.template = template
        self.worksheets = OrderedDict([(n,None) for n in worksheet_names])
        self.date_style = date_style
            
    def create_worksheet(self,name):
        self.worksheets[name] = worksheet.Worksheet()
        return self.worksheets[name]
        
    @property
    def worksheet_names(self):
        return self.worksheets.keys()
    
    def save(self, filename):
        replacement = {}
        for (idx,ws) in enumerate(self.worksheets.itervalues()):
            if ws:
                sheet_path = '%s/sheet%d.xml' % (PACKAGE_WORKSHEETS, idx + 1)
                replacement[sheet_path] = util.to_xml(ws, self)
        with util.open_archive(filename,'w') as write_archive:
            with util.open_archive(self.template,'r') as read_archive:
                for name in read_archive.namelist():
                    try:
                        data = replacement[name]
                    except KeyError:
                        data = read_archive.read(name)
                        # remove all pre-calculated formula values from worksheets
                        if name.startswith('%s/sheet' % PACKAGE_WORKSHEETS):
                            data = clean_formula_values(data)
                    write_archive.writestr(name, data)
    
def read_content_types(xml_source):
    """Read content types."""
    root = fromstring(xml_source)
    contents_root = root.findall('{%s}Override' % CONTYPES_NS)
    for t in contents_root:
        yield t.get('PartName'), t.get('ContentType')

def read_sheets_titles(xml_source):
    """Read titles for all sheets."""
    root = fromstring(xml_source)
    titles_root = root.find('{%s}sheets' % SHEET_MAIN_NS)

    return [sheet.get('name') for sheet in titles_root]

def read_date_style(xml_source):
    root = fromstring(xml_source)
    root = root.find('{%s}cellXfs' % SHEET_MAIN_NS)
    for (idx, style) in enumerate(root):
        if style.get('numFmtId') == '14':
            return idx
    raise Exception('Date Style not Present in Sheet')
    
def load(filename):
    """
    Open the given file and return a work book. 
    
    The only thing we need is the worksheets that are valid worksheets we could possibly update
    """
    with util.open_archive(filename, 'r') as archive:
        content_types = read_content_types(archive.read(ARC_CONTENT_TYPES))
        sheet_types = [(sheet, contyp) for sheet, contyp in content_types 
                       if contyp in WORK_OR_CHART_TYPE]
        workbook_xml = archive.read(ARC_WORKBOOK)
        sheet_names = read_sheets_titles(workbook_xml)
        worksheet_names = [worksheet for worksheet, sheet_type in zip(sheet_names, sheet_types) 
                              if sheet_type[1] == VALID_WORKSHEET]
        date_style = read_date_style(archive.read(ARC_STYLE))
        return WorkbookTemplate(filename, worksheet_names, date_style)
