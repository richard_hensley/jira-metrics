from zipfile import ZIP_DEFLATED, ZipFile
from StringIO import StringIO
from collections import OrderedDict
from xml.sax.saxutils import XMLGenerator
from xml.sax.xmlreader import AttributesNSImpl

def open_archive(filename, mode):
    return ZipFile(filename, mode, ZIP_DEFLATED)
  
def to_xml(n, parms):  
    try:
        out = StringIO()
        doc = XMLGenerator(out,'utf-8')
        n.xml(doc, parms)
        return out.getvalue()
    finally:
        out.close
        
def start_tag(doc, name, attr=None, body=None, namespace=None):
    """Wrapper to start an xml tag."""
    if attr is None:
        attr = {}
        dct_type = dict
    elif isinstance(attr, OrderedDict):
        dct_type = OrderedDict
    else:
        dct_type = dict

    attr_vals = dct_type()
    attr_keys = dct_type()
    for key, val in attr.items():
        key_tuple = (namespace, key)
        attr_vals[key_tuple] = str(val)
        attr_keys[key_tuple] = str(key)
    attr2 = AttributesNSImpl(attr_vals, attr_keys)
    doc.startElementNS((namespace, name), name, attr2)
    if body:
        doc.characters(body)

def end_tag(doc, name, namespace=None):
    """Wrapper to close an xml tag."""
    doc.endElementNS((namespace, name), name)

def tag(doc, name, attr=None, body=None, namespace=None):
    """Wrapper to print xml tags and comments."""
    if attr is None:
        attr = {}
    start_tag(doc, name, attr, body, namespace)
    end_tag(doc, name, namespace)
    
def tags(doc, tags):
    for tag in tags:
        name = tag[0]
        attr = tag[1] if len(tag) > 1 else None
        body = tag[2] if len(tag) > 2 else None
        ns = tag[3] if len(tag) > 3 else None
        start_tag(doc, name, attr, body)
    for tag in reversed(tags):
        name = tag[0]
        ns = tag[3] if len(tag) > 3 else None
        end_tag(doc, name, ns)
