
import datetime
from util import tags
import re

class Cell(object):

    __slots__ = ('value', 'coordinate')

    def __init__(self, coordinate):
        self.value = None
        self.coordinate = coordinate
    
    @property
    def typed_value(self):
        if isinstance(self.value,(float,int)):
            return float(self.value)
        if isinstance(self.value,(datetime.datetime, datetime.date)):
            return self.value
        try:
            return float(self.value)
        except (TypeError, ValueError):
            pass
        try:
            return datetime.datetime.strptime(self.value,'%m/%d/%Y').date()
        except (TypeError, ValueError):
            pass
        return self.value
    
    def xml(self, doc, parms):
        v = self.typed_value
        if v is None:
            return
        attr = { 'r' : self.coordinate }
        el = [('c', attr)]
        if isinstance(v, float):
            el.append(('v', None, str(v)))
        elif isinstance(v, (datetime.date, datetime.datetime)):
            attr['t'] = 'd'
            attr['s'] = parms.date_style
            el.append(('v', None, str(v.isoformat())))
        else:
            if str(v) == '':
                return
            attr['t'] = 'inlineStr'
            el.append(('is', ))
            el.append(('t', None, str(v)))
        tags(doc, el)