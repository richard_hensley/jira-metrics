'''
This module is used to open an excel workbook and allow new sheets to be made.
The new sheets are saved along with all the contents of the original sheet.
'''