#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Check all the selected features for conformance to data rules


'''
import sys
import os
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.standardargs import standardmain


def addargs(parser):
    pass


def processor(args):
    ticket_count = 0
    error_count = 0
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        for w in map(ticket_wrapper_factory, filter(lambda x: x.is_feature, tickets)):
            ticket_count += 1
            errors = []
            if w.is_closed and not w.start_date:
                errors.append('Feature start date is missing.')
                errors.append(
                    '  This can be fixed by setting the Start Version.')
            if w.is_closed and not w.due_date:
                errors.append('Feature due date is missing.')
                if not w.fix_version_name:
                    errors.append(
                        '  This can be fixed by setting the Fixed Version.')
                else:
                    errors.append(
                        '  The %s fix version does not have a date set in the mapping table.' %
                        (w.fix_version_name))
            if errors:
                error_count += 1
                print 'https://ticket.opower.com/browse/%s' % (w.ticket.key)
                for e in errors:
                    print ' ' * 4, e
    print "Ticket Count = %(ticket_count)i, Error Count = %(error_count)i" % locals()
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
