#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Load the initiatives tickets and filter out the ones for a specific project
and write them to a ticket file
'''
import sys
import os
from utils.standardargs import standardmain
from utils.utils import parse_ticket_file, save_ticket_file,\
    ticket_wrapper_factory, InitiativeWrapper, FeatureWrapper, WorkWrapper
import datetime
import logging

LOG = logging.getLogger()

def addargs(parser):
    parser.add_argument('-p','--project_filter', dest='projects', 
                        action='append', required = True)
    parser.add_argument('-o', '--output', dest='output', required=True)

def do_map(t):
    return ticket_wrapper_factory(t)

def do_filter(w, projects):
    if isinstance(w, (InitiativeWrapper, FeatureWrapper, WorkWrapper)):
        if w.project in projects:
            return True
    return False

def processor(args):
    projects = set(args.projects)
    tickets = []
    source_files = set()
    for fn in args.infile:
        # skip the output file
        if os.path.basename(fn) == os.path.basename(args.output):
            continue
        temp = parse_ticket_file(fn)
        for t in temp:
            w = ticket_wrapper_factory(t)
            if do_filter(w, projects):
                tickets.append(t._json)
                source_files.add(fn)
    
    info = {
            'msg' : 'Tribal Initiatives and Feature',
            'source files' : list(source_files),
            'dest file' : args.output,
            'projects' : args.projects,
            'date' : datetime.datetime.now().isoformat()
            }
    save_ticket_file(args.output, info, tickets)
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
