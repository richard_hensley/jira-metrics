#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Update an excel template workbook with data, and write the contents back to a new file

This program does the following:

 * Open the excel workbook template
 * For each sheet in the workbook:
 *   If a data file exists with the sheet name with the data dir prepended, and the suffix appended
 *     replace the contents of the sheet with the data
 * Output the new workbook into the output file name

'''

import os
import sys
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from tempfile import mkdtemp
import shutil
import xl.workbook
from glob import glob
from utils.standardargs import process_logging_args
import logging

LOG = logging.getLogger()

if __name__ == '__main__':
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)

    parser = ArgumentParser(
        description=desc, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('-t', '--template', dest='templates', action='append', 
                        required=True)
    parser.add_argument('-d', '--data_dir', dest='data_dir', required=True)
    parser.add_argument('-s', '--data_suffix', dest='data_suffix', 
                        required=True)
    parser.add_argument('-o', '--output_dir', dest='output_dir', required=True)
    parser.add_argument('-l', '--log', dest='logging', 
                        choices=('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'),
                        default='INFO')
    parser.add_argument('--log_to', dest='log_to')
    args = parser.parse_args()
    process_logging_args(args)

    templates = []
    for template_glob in args.templates:
        templates.extend(glob(template_glob))
    for template in templates:
        LOG.debug('process template %s', template)
        wb = xl.workbook.load(template)
        for name in wb.worksheet_names:
            LOG.debug('Sheet Name %(name)s',locals())
            if name.find('_') == -1:
                continue
            data_file = os.path.join(args.data_dir,
                '%s_%s.txt' % (name, args.data_suffix))
            try:
                with open(data_file, 'r') as f:
                    lines = f.readlines()
            except IOError:
                raise Exception('Sheet "%(name)s" does not map to a valid data file' % locals())
            LOG.debug('%(name)s worksheet being updated with contents of %(data_file)s',locals())
            ws = wb.create_worksheet(name)
            for (row, line) in enumerate(lines):
                data = map(lambda s: s.strip(), line.split('\t'))
                for (column, value) in enumerate(data):
                    cell = ws.cell(row, column).value = value
        (template_name, ext) = os.path.splitext(os.path.basename(template))
        output_file = os.path.join(args.output_dir,
            '%s_%s%s' % (template_name, args.data_suffix, ext))
        LOG.debug("%s template transformed to %s", template, output_file)
        wb.save(output_file)
