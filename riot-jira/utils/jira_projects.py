
from json_objects import JsonClass
import json

class JiraProjects(object):

    project_prop_map = {
            'key' : (None, None, 'key'),
            'name' : (None, None, 'name'),
            }

    def __init__(self, filename  = 'data/jira_projects.json'):
        with open(filename,'r') as fp:
            projects = json.load(fp)
        self.by_key = {}
        self.by_name = {}
        for project in projects:
            p = JsonClass(project, JiraProjects.project_prop_map)
            self.by_key[p.key] = p
            self.by_name[p.name] = p
            
    def name_to_key(self, name):
        try:
            return self.by_name[name].key
        except KeyError:
            pass
        return None
    
    def key_to_name(self, key):
        try:
            return self.by_key[key].name
        except KeyError:
            pass
        return None