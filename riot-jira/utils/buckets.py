# encoding: utf-8

import dateutil.relativedelta
import datetime
from utils import sprint_map, dates
from dates import to_date

ONEDAY = datetime.timedelta(days=1)

def bucketer_factory(mode, grabber):
    if mode == 'weekly':
        return WeeklyBucketer(grabber)
    elif mode == 'monthly':
        return MonthlyBucketer(grabber)
    elif mode == 'quarterly':
        return QuarterlyBucketer(grabber)
    elif mode == 'daily':
        return DailyBucketer(grabber)
    elif mode == 'sprints':
        return SprintsBucketer(grabber)
    raise Exception("Invalid bucket mode %(mode)s" % locals())
    
class Bucketer(object):
    def __init__(self, grabber):
        self.grabber = grabber

    def buckets(self, end, back):
        start = self.back(end, back)
        return [b for b in self.generate_buckets(start, end)]
    
    def generate_buckets(self, start, end):
        temp = set()
        while start <= end:
            b = self.bucket(start)
            if b not in temp:
                temp.add(b)
                yield b
            start += ONEDAY            
    
    def grab_bucket_date(self, w):
        try:
            return self.grabber(w)
        except TypeError:
            return w
    
    def use_grabber(self, w):
        if isinstance(w, datetime.datetime):
            return w
        if isinstance(w, datetime.date):
            return w
        return self.grab_bucket_date(w)
    
    def back(self, d, b):
        raise Exception()

    def bucket(self, d):
        raise Exception()
    
class QuarterlyBucketer(Bucketer):
    def __init__(self, grabber = None):
        Bucketer.__init__(self, grabber)
    
    def bucket(self, d):
        d = self.use_grabber(d)
        return '%dQ%d' % (d.year, int((d.month-1)/3)+1)
    
    def start(self, from_date):
        from_date = MonthlyBucketer.calc_month_start(from_date)
        back = dateutil.relativedelta.relativedelta(months=1)
        while from_date.month not in (1,4,7,10):
            from_date = from_date - back
        return from_date
    
    def back(self, from_date, quarters_back):
        #goto beginning of this quarter
        from_date = self.start(from_date)
        #go back quarters
        return MonthlyBucketer.calc_month_back(from_date,quarters_back*3)

class MonthlyBucketer(Bucketer):
    def __init__(self, grabber = None):
        Bucketer.__init__(self, grabber)
    
    def bucket(self, d):
        d = self.use_grabber(d)
        return '%dM%02d' % (d.year, d.month)
    
    def start(self, d):
        return MonthlyBucketer.calc_month_start(d)
    
    @staticmethod
    def calc_month_start(d):
        return d.replace(day=1)
    
    def back(self, from_date, months_back):
        return MonthlyBucketer.calc_month_back(from_date, months_back)
    
    @staticmethod
    def calc_month_back(from_date, months_back):
        return MonthlyBucketer.calc_month_start(from_date - dateutil.relativedelta.relativedelta(months=months_back))

class WeeklyBucketer(Bucketer):
    def __init__(self, grabber = None):
        Bucketer.__init__(self, grabber)
        self.sprints = SprintsBucketer(grabber)

    def bucket(self, d):    
        d = self.use_grabber(d)
        i = d.isocalendar()
        b = self.sprints._raw_bucket(d)
        return '%dW%02d %s' % (i[0], i[1], b)
    
    def start(self, from_date):
        while from_date.weekday() != 6:
            from_date = from_date - ONEDAY
        return from_date

    def back(self, from_date, weeks_back):    
        from_date = self.start(from_date)
        from_date = from_date - datetime.timedelta(days=(weeks_back*7))
        return from_date

class DailyBucketer(Bucketer):
    def __init__(self, grabber = None):
        Bucketer.__init__(self, grabber)

    def bucket(self, d):    
        d = self.use_grabber(d)
        return to_date(d).isoformat()
    
    def start(self, d):
        return d
    
    def back(self, from_date, days_back):    
        return from_date - datetime.timedelta(days_back)

class SprintsBucketer(Bucketer):
    def __init__(self, grabber = None):
        Bucketer.__init__(self, grabber)
        m = sprint_map.get_sprint_map()
        # grab the sprint name, start date, and due date for each sprint
        self._ranges = filter(lambda x:x[1],map(lambda s:[s, m.start_date(s), m.due_date(s), m.quarter(s)], 
                                                m.sprints()))
        # ensure they are in start date order
        self._ranges.sort(key=lambda x:x[1])
        # ensure that all start dates are mondays
        # this is to fix up beginning of the year sprint stuff
        # no work generally occurs, so it won't make a difference
        for s in self._ranges:
            while s[1].isoweekday() != 1:
                s[1] -= ONEDAY
        # ensure that the end range of a sprint is one day before the start of the
        # next sprint. This has the effect of including leap week in the sprint
        # just before leap week
        for i in range(len(self._ranges) - 1):
            self._ranges[i][2] = self._ranges[i+1][1] - ONEDAY
    
    def _find_range_idx(self, d):
        if d:
            for (i, sprint) in enumerate(self._ranges):
                if d >= sprint[1] and d <= sprint[2]:
                    return i
        raise KeyError('Unable to bucket %s into a sprint' % (dates.date_to_str(d), ))

    def _find_data(self, d, i):
        try:
            return self._ranges[self._find_range_idx(d)][i]
        except KeyError:
            pass
        return None
    
    def _raw_bucket(self, d):
        return self._find_data(d, 0)
    
    def bucket(self, d):
        d = self.use_grabber(d)
        b = self._raw_bucket(d)
        if b:
            return '%s %s' % (b, self._find_data(d, 3))
        return None
    
    def start(self, d):
        return self._find_data(d, 1)
    
    def back(self, d, sprints_back):
        try:
            idx = self._find_range_idx(d) - sprints_back
            if idx >= 0:
                return self._ranges[idx][1]
        except KeyError:
            pass
        return None

def make_bucket_grabber(t):
    if t == 'due':
        return lambda w:w.due_date
    if t == 'delivery':
        return lambda w:w.delivery_date
    if t == 'start':
        return lambda w:w.start_date
    return lambda w:w


