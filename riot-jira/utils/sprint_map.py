import datetime
from utils import dates

class SprintMap(object):
    
    quarter_month = { 'Q1' : 1, 'Q2' : 4, 'Q3' : 7, 'Q4' : 10}
    
    def __init__(self,sprints,alias):
        self._sprints = sprints
        self._alias = alias
     
    def sprints(self):
        return self._sprints.keys()
       
    def map_alias(self,name):
        try:
            name = self._alias[name]
        except KeyError:
            pass
        return name
    
    def map(self,name):
        name = self.map_alias(name)
        try:
            return self._sprints[name]
        except KeyError:
            pass
        return None
    
    def value(self, name, field):
        try:
            return self.map(name)[field]
        except (KeyError, TypeError):
            pass
        return None
    
    def due_date(self,name):
        return self.value(name,'Due Date')
    
    def start_date(self, name):
        return self.value(name, 'Start Date')
    
    def planning_end(self, name):
        year = self.value(name,'Year')
        q = self.value(name,'Quarter')
        try:
            month = SprintMap.quarter_month[q]
            return datetime.date(year, month, 1) - datetime.timedelta(days = 1)
        except KeyError:
            pass
        return None
    
    def quarter(self, name):
        year = self.value(name,'Year')
        q = self.value(name,'Quarter')
        return '%d%s' % (year, q)
    
    @staticmethod
    def parse_sprint_map(mapping,alias):
        sprints = {}
        with open(mapping,"r") as f:
            lines = f.readlines()
        fields = [f.strip() for f in lines[0].split('\t')]
        for line in lines[1:]:
            data = [d.strip() for d in line.split('\t')]
            row = {}
            for (f, d) in zip(fields, data):
                try:
                    v = int(d)
                except:
                    v = dates.str_to_date(d)
                    if not v:
                        v = d
                row[f] = v
            fv = row['Fixversion']
            sprints[fv] = row
        with open(alias,'r') as f:
            lines = f.readlines()
        aliases = {}
        for line in lines:
            data = [d.strip() for d in line.split('\t')]
            if sprints[data[1]]:
                aliases[data[0]] = data[1]
        SprintMap.sprint_map = SprintMap(sprints, aliases)

def get_sprint_map():
    return SprintMap.sprint_map

def map_due_date(name):
    return SprintMap.sprint_map.due_date(name)

def map_start_date(name):
    return SprintMap.sprint_map.start_date(name)

def map_planning_end(name):
    return SprintMap.sprint_map.planning_end(name)

def map_sprint(name):
    return SprintMap.sprint_map.map(name)

def map_alias(name):
    return SprintMap.sprint_map.map_alias(name)



