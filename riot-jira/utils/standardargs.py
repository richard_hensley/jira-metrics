# encoding: utf-8
'''
The standard arguments module defines all the funcions to add and parse standard
command line arguments used by most of the metrics suite programs.
'''
from argparse import ArgumentParser, RawDescriptionHelpFormatter
import ticket
import sys
from glob import glob
import sprint_map
import buckets
import json
from compiler.ast import flatten
import logging
import os

def standard_args(program):
    # Setup argument parser
    parser = ArgumentParser(description=program, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('--sprint_mapping', dest='sprint_mapping', default='config/sprint_mappings.txt')
    parser.add_argument('--spring_alias', dest='sprint_alias_file', default='config/sprint_alias_mapping.txt')    
    parser.add_argument('--team_project_config', dest='team_project_file', default='config/all_projects.txt')
    parser.add_argument('-x', '--config', dest='configs')
    parser.add_argument(dest="infile", metavar="FILE", nargs='+')
    add_logging_args(parser)
    return parser

def add_logging_args(parser):
    parser.add_argument('-l', '--log', dest='logging', 
                        choices=('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'),
                        default='INFO')
    parser.add_argument('--log_to', dest='log_to')
    
def add_common_args(parser, which):
    for w in which:
        if w == '-c':
            parser.add_argument('-c', '--columns', dest='columns', required=True, action='append')
        elif w == '-m':    
            parser.add_argument('-m', '--mode', dest='mode', 
                                choices=['daily', 'weekly', 'monthly', 'quarterly', 'sprints'], 
                                required=True)
        elif w == '-y':
            parser.add_argument('-y', '--bucket_by', dest='bucket_by', 
                                choices = ['due', 'delivery', 'start'], 
                                required=True)
        elif w == '-b':
            parser.add_argument('-b', '--back', dest='back', type = int, required=True)
        elif w == '-t':
            parser.add_argument('-t', '--type', dest='types', action='append', required=True)
        else:
            raise Exception('Unsupported common arg %(w)s' % locals())
    
def process_config_args(args):
    try:
        configs = flatten(map(glob,args.configs))
        for fn in configs:
            with open(fn,'r') as fp:
                c = json.load(fp)
                for (k, v) in c.iteritems():
                    setattr(args, k, v)
    except TypeError:
        pass

def process_standard_args(args):
    process_config_args(args)
    args.infile = flatten(map(glob,args.infile))
    sprint_map.SprintMap.parse_sprint_map(args.sprint_mapping, args.sprint_alias_file)
    ticket.Ticket.parse_team_file(args.team_project_file)
    try:
        types = []
        for t in args.types:
            if t == 'AllWork':
                types.extend(('User Story', 'Task', 'Technical Debt', 'Bug'))
            else:
                types.append(t)
        args.types = types
    except AttributeError:
        pass
    try:
        bucket_by = args.bucket_by
    except AttributeError:
        bucket_by = None
    try:
        args.bucketer = buckets.bucketer_factory(args.mode,buckets.make_bucket_grabber(bucket_by))
    except AttributeError:
        pass
    process_logging_args(args)
    
def process_logging_args(args):
    lvl = getattr(logging, args.logging.upper(), None)
    if not isinstance(lvl, int):
        raise ValueError('Invalid log level: %s' % args.logging)
    if args.log_to:
        if args.log_to == 'STDERR':
            log_file = None
        else:
            log_file = args.log_to
    else:
        log_file = os.path.basename(sys.argv[0]) + '.log'
    logging.basicConfig(level=lvl, filename=log_file, filemode='w')
    LOG = logging.getLogger(__name__)
    for a in dir(args):
        if a.startswith('_'):
            continue
        LOG.debug('arg %s = %s', a, str(getattr(args, a)))
        

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg
    def __repr__(self):
        return self.msg
    
def standardmain(addargs, processor, name, argv=None):
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    parser = standard_args(name)
    addargs(parser)        
    args = parser.parse_args()
    process_standard_args(args)
    infile = args.infile
    if not infile: 
        raise CLIError("infile required")
    processor(args)
    return 0
