import datetime
import dateutil.parser

def _parse(data):
    if data:
        try:
            data = dateutil.parser.parse(data, ignoretz = True)
        # eat a weird exception from the date parser
        except TypeError:
            data = None
    return data

def str_to_date(data):
    if isinstance(data, datetime.datetime):
        return data.date()
    if isinstance(data, datetime.date):
        return data
    data = _parse(data)
    if data:
        data = data.date()
    return data
to_date = str_to_date
    
def str_to_datetime(data):
    if isinstance(data, datetime.datetime):
        return data
    if isinstance(data, datetime.date):
        return datetime.datetime(data.year, data.month, data.day)
    return _parse(data)
to_datetime = str_to_datetime

def date_to_str(data):
    data = str_to_date(data)
    if data:
        return data.strftime('%m/%d/%Y')
    return ''

def datetime_to_str(data):
    data = str_to_datetime(data)
    if data:
        return data.strftime('%m/%d/%Y %H:%M')
    return ''
 
def select_(selector,factory,data):
    # string is special case that needs to be handled even though it is iterable
    if isinstance(data,basestring):
        return factory(data)
    try:
        data = filter(None,map(factory,data))
        if data:
            return selector(data)
    except TypeError:
        return factory(data)
    return None
    
def select_datetime(selector,data):
    return select_(selector,str_to_datetime,data)

def select_date(selector,data):
    return select_(selector,str_to_date,data)

def calc_days_between(start, end):
    start = select_datetime(min, start)
    end = select_datetime(max, end)
    if start and end:
        return (end - start).total_seconds() / (3600.0 * 24.0)
    return 0.0
