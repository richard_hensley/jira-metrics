import json
import dates
import ticket
import datetime
import sprint_map
import jira_projects
from dates import to_date

ONEDAY = datetime.timedelta(days=1)

def str_str(s):
    if s == 'None':
        return None
    return s

def first(data):
    try:
        for d in data:
            if d:
                return d
    except:
        return None

class TicketWrapper(object):
    def __init__(self,ticket):
        self.ticket = ticket
      
    @property
    def project(self):
        return self.ticket.project
          
    @property
    def key(self):
        return self.ticket.key
    
    @property
    def due_date(self):
        raise Exception()
    
    @property
    def delivery_date(self):
        raise Exception()

    @property
    def start_date(self):
        raise Exception()

    @property  
    def lead_time(self):
        if self.start_date and self.delivery_date:
            return dates.calc_days_between(self.start_date, self.delivery_date)
        return None

    @property
    def delivery_delta(self):
        if self.due_date and self.delivery_date:
            return dates.calc_days_between(self.due_date,self.delivery_date)
        return None
    
    @property
    def delivery_variance(self):
        if self.delivery_delta and self.lead_time:
            return self.delivery_delta / self.lead_time
        return None
    
    @property
    def is_closed(self):
        return self.ticket.is_closed
    
    def is_wip_on(self, when):
        if self.start_date and when < self.start_date:
            return False
        if self.delivery_date and when >= self.delivery_date:
            return False
        return self.specific_is_wip_on(when)
    
    def wip_days(self):
        if self.start_date:
            done = self.delivery_date if self.delivery_date else datetime.date.today()
            return self.start_date, done
        return None
    
    @property
    def fix_version_due_date(self):
        try:
            return self.ticket.fix_versions[0].map_due_date
        except (IndexError,TypeError):
            pass
        return None
    
    @property
    def fix_version_release_date(self):
        try:
            return self.ticket.fix_versions[0].release_date
        except (IndexError,TypeError):
            pass
        return None
    
    @property 
    def fix_version_name(self):
        try:
            return sprint_map.map_alias(self.ticket.fix_versions[0].name)
        except (IndexError, TypeError):
            pass
        return None

    def __str__(self):
        return str(self.ticket)
                   
class InitiativeWrapper(TicketWrapper):
    
    @staticmethod
    def get_project_map():
        try:
            return InitiativeWrapper.PROJECT_MAP
        except AttributeError:
            InitiativeWrapper.PROJECT_MAP = jira_projects.JiraProjects()
        return InitiativeWrapper.PROJECT_MAP
    
    def __init__(self, ticket):
        if not ticket.is_initiative:
            raise Exception('Trying to wrap wrong ticket type')
        TicketWrapper.__init__(self, ticket)

    @property
    def project(self):
        return self.scrum_team_project
    
    @property
    def scrum_team_project(self):
        return InitiativeWrapper.get_project_map().name_to_key(self.ticket.scrum_team)
    
    WIP_STATUS = set(('Scheduled', 'Dev Ready', 'Execution', 'On Roadmap', 'In Development'))
    
    @property
    def start_date(self):
        l = map(self.ticket.first_arrival_date, InitiativeWrapper.WIP_STATUS)
        l.append(self.ticket.start_version.release_date)
        return dates.select_date(min, l)

    def specific_is_wip_on(self, when):
        status = self.ticket.status_on_date(when)
        if status in InitiativeWrapper.WIP_STATUS:
            return True
        return False
    
    @property
    def due_date(self):
        return dates.select_date(first, [self.fix_version_release_date, self.ticket.commitment_due_date])

    @property
    def delivery_date(self):
        return self.ticket.general_delivery_date
    
class FeatureWrapper(TicketWrapper):
    def __init__(self, ticket):
        TicketWrapper.__init__(self, ticket)
        if not ticket.is_feature:
            raise Exception('Trying to wrap wrong ticket type')

    @property
    def child_ticket_keys(self):
        result = []
        for link in self.ticket.links:
            if link.type == 'Feature Composition':
                key = link.outward_issue.key
                if key:
                    result.append(key)
        return result

    @property
    def child_start_date(self):
        l = []
        # go through all the children, find the date when the link was created between the child and this feature
        for key in self.child_ticket_keys:
            try:
                child = ticket.Ticket.all_tickets[key]
                for history in child.histories:
                    for item in history.items:
                        if item.field in ('Feature Link', 'Link'):
                            l.append(history.created)
            except:
                pass
        return dates.select_date(min, l)
    
    WIP_STATUS = set(('Release Backlog', 'Decomposition', 'Dev Ready', 'Build',
                     'Demo', 'Ready for Delivery',
                     # old statuses
                     'Stub', 'Scheduled', 'Ready for Dev', 'In Progress'))
        
    @property
    def start_status_date(self):
        return dates.select_date(first, map(self.ticket.first_arrival_date,
            FeatureWrapper.WIP_STATUS))
    
    @property
    def start_date(self):
        return dates.select_date(first,(        
                                self.start_status_date, 
                                self.ticket.start_version.map_start_date,
                                self.child_start_date))

    def specific_is_wip_on(self, when):
        status = self.ticket.status_on_date(when)
        if status in FeatureWrapper.WIP_STATUS:
            return True
        if self.start_date:
            if self.delivery_date:
                if when >= self.start_date and when < self.delivery_date:
                    return True
            elif when >= self.start_date:
                return True
        return False
    
    @property
    def due_date(self):
        if self.planned_fix_version_due_date:
            # planned work due date
            return self.planned_fix_version_due_date
        else:
            # unplanned work due date
            return self.fix_version_due_date
        
    @property
    def delivery_date(self):
        return self.ticket.general_delivery_date
    
    @property
    def is_planned(self):
        planned_name = self.planned_fix_version_name
        fv_name = self.fix_version_name
        if not planned_name or not fv_name or planned_name != fv_name:
            return False
        return True
    
    @property
    def delivered_as_planned(self):
        if self.is_planned and self.delivery_date and self.due_date:
            if dates.to_date(self.delivery_date) <= dates.to_date(self.due_date):
                return True
        return False
    
    @property
    def planned_fix_vesion_name(self):
        try:
            return sprint_map.map_alias(self.planned_fix_version.name)
        except TypeError:
            pass
        return None
    
    @property
    def planned_fix_version_due_date(self):
        try:
            return self.planned_fix_version.map_due_date
        except AttributeError:
            pass
        return None
    
    @property
    def planned_fix_version_name(self):
        try:
            return self.planned_fix_version.name
        except AttributeError:
            pass
        return None
        
    @property
    def planned_fix_version(self):
        '''
        The planned fix version is the last fix version that was set before the planning 
        window for the ticket closed.
        '''
        try:
            return self._planned_fix_version
        except:
            self._planned_fix_version = None
            end = self.planning_end_date
            if end:
                for fv in self.ticket.fix_version_changes:
                    if fv.maps_to_sprint and dates.to_date(fv.when) <= end:
                        self._planned_fix_version = fv
                    else:
                        break
            return self._planned_fix_version
            
    @property 
    def planning_anchor(self):
        '''
        The planning anchor is the first fix version that was set on the ticket that maps to a sprint.
        This sets the planning window for the ticket.
        '''
        try:
            return self._planning_anchor
        except AttributeError:
            self._planning_anchor = None
            for fv in self.ticket.fix_version_changes:
                if fv.maps_to_sprint:
                    self._planning_anchor = fv
                    return self._planning_anchor
            return self._planning_anchor
        
    @property
    def planning_end_date(self):
        try:
            return self._planning_end_date
        except:
            try:
                self._planning_end_date = self.planning_anchor.map_planning_end
            except:
                self._planning_end_date = None
            return self._planning_end_date
   
class WorkWrapper(TicketWrapper):
    def __init__(self, ticket):
        if not ticket.is_work:
            raise Exception('Trying to wrap wrong ticket type')
        TicketWrapper.__init__(self, ticket)
    
    @property
    def delivery_date(self):
        return self.ticket.general_delivery_date

    @property
    def due_date(self):
        return None

    WIP_STATUS = set(('Coding', 'Code Review', 'Ready for Testing', 'Testing', 'In Progress'))
    
    @property
    def start_date(self):
        d = dates.select_date(first, map(self.ticket.first_arrival_date, 
                    WorkWrapper.WIP_STATUS))
        if d:
            return d
        if dates.calc_days_between(self.ticket.created, self.delivery_date) <= 5.0:
            return to_date(self.ticket.created)
        return None

    def specific_is_wip_on(self, when):
        status = self.ticket.status_on_date(when)
        if status in WorkWrapper.WIP_STATUS:
            return True
        return False
    
def ticket_wrapper_factory(ticket):
    if ticket.is_initiative:
        return InitiativeWrapper(ticket)
    if ticket.is_feature:
        return FeatureWrapper(ticket)
    if ticket.is_work:
        return WorkWrapper(ticket)
    return None
            
def parse_ticket_info(name):
    try:
        header = ''
        with open(name,"r") as f:
            for line in f:
                #skip lines until we find the start of data marker
                if line == "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n":
                    #indicate header found
                    break
                header += line
        if len(header) > 0:
            return json.loads(header)
    except:
        pass
    return {}
    
def ticket_dictionary(tickets):
    result = {}
    for t in tickets:
        result[t.key] = t 
    ticket.Ticket.all_tickets = result

def parse_ticket_file(name):
    oldest = datetime.datetime.now() - datetime.timedelta(days=549)
    result = []
    in_data = False
    with open(name,"r") as f:
        for line in f:
            # skip lines until we find the start of data marker
            if line == "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n":
                # indicate header found
                in_data = True
                continue
            if not in_data:
                continue
            line = line.strip()
            if len(line) > 0:
                d = json.loads(line)
                if d:
                    t = ticket.Ticket(d)
                    # filter out old tickets, and tickets that have bogus resolutions
                    if t.updated < oldest:
                        continue
                    if t.resolution and t.resolution not in ('Fixed', 'Done'):
                        continue
                    # exclude features with a start version of "Pre V3.31.x"
                    if t.old_stuff():
                        continue
                    result.append(t)
    ticket_dictionary(result)
    return result

def save_ticket_file(outfile, info, tickets):
        with open(outfile, "w") as of:
            of.write(json.dumps(info, indent=4, separators=(',', ': ')))
            of.write("\n")
            of.write('*-' * 30)
            of.write("\n")
            for t in tickets:
                of.write(json.dumps(t, separators=(',', ':')))
                of.write("\n")

ZERO = datetime.timedelta(0)
HOUR = datetime.timedelta(hours=1)

# A UTC class.

class UTC(datetime.tzinfo):
    """UTC"""

    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO

utc = UTC()

# A class building tzinfo objects for fixed-offset time zones.
# Note that FixedOffset(0, "UTC") is a different way to build a
# UTC tzinfo object.

class FixedOffset(datetime.tzinfo):
    """Fixed offset in minutes east from UTC."""

    def __init__(self, offset, name):
        self.__offset = datetime.timedelta(minutes = offset)
        self.__name = name

    def utcoffset(self, dt):
        return self.__offset

    def tzname(self, dt):
        return self.__name

    def dst(self, dt):
        return ZERO

class WipDay(object):
    def __init__(self, when):
        self.when = when
        self.wip = {}
        
    def inc_wip(self, status):
        if status:
            if not self.wip.has_key(status):
                self.wip[status] = 0
            self.wip[status] += 1
            return self.wip[status]
        return 0
    
    def get_wip(self, status):
        if status and self.wip.has_key(status):
            return self.wip[status]
        return 0
    
    @property
    def get_total(self):
        return sum(v for v in self.wip.itervalues())