#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Analyze the WIP of selected tickets and column by reportin the summarized max for agiven ticket period.

Wip analysis gives you information about the maximum amount of work in process during a given time period.
This is useful when trying to figure out if a particular team working in a particular context is overloaded
by work in process.

The program takes the following parameters.

  Ticket Type(s)
  Bucketing Scheme (weekly, monthly, quarterly)
  How many buckets back
  Column(s) (a.k.a. Statuses)
  Data File(s)
  
The program will print out a table with each row is for a bucket, and the columns are the statuses specified. The
data in the columns is the maximum amount of WIP in that column for that bucket. This script also adds a total_wip
column that is the sum of the WIP.

python analyze_wip.py -t"User Story" -tTask -tBug -t"Technical Debt" -mweekly -b5 -cCoding -c"Code Review" -c"Ready for Testing" -cTesting data/DS_data.txt

The output will look something like. (There is a tab between the weekly and closed numbers.
This makes it easy to read or paste into Excel.)

weekly  coding  code_review     ready_for_testing       testing total_wip
201405  9       5               2                       0       16
201406  10      8               1                       1       20
201407  9       6               3                       2       20
201408  12      7               1                       3       23
201409  10      7               2                       6       25
201410  13      5               2                       3       23
201411  8       7               3                       3       21
'''
import sys
import os
from datetime import date
from utils.standardargs import standardmain, add_common_args
from utils.utils import parse_ticket_file, ticket_wrapper_factory
import logging
from collections import OrderedDict
from utils.buckets import ONEDAY
from numpy.core.fromnumeric import mean
from utils.stats import percentile
from utils.dates import to_date

LOG = logging.getLogger()

def addargs(parser):
    add_common_args(parser, ('-m', '-b', '-t'))
    parser.add_argument('-g','--target', dest='target', type=int, default=0)

def do_filter(w, types):
    if not w or not w.ticket.issue_type in types or not w.start_date:
        return False
    return True

def processor(args):
    buckets = OrderedDict()
    for b in args.bucketer.buckets(date.today(), args.back)[:-1]:
        buckets[b] = OrderedDict()

    for fn in args.infile:
        for w in filter(lambda x: do_filter(x, args.types), 
                        map(ticket_wrapper_factory, parse_ticket_file(fn))):
            # get the start and end date the work is WIP
            start, end = w.wip_days()
            while start < end:
                bucket = args.bucketer.bucket(start)
                if w.is_wip_on(start):
                    try:
                        b = buckets[bucket]
                        LOG.debug('ticket %s bucket %s when %s', str(w), str(bucket), start.isoformat())
                        try:
                            b[start] += 1
                        except KeyError:
                            b[start] = 1                
                    except KeyError:
                        pass
                start += ONEDAY

    print '\t'.join((args.mode, 'min_wip','max_wip', 'avg_wip', '90th_pct', 'first', 'last', 'target'))
    for when, wip in buckets.iteritems():
        values = wip.values()
        if values:
            first = values[0]
            last = values[-1]
            values.sort()
            out = [when, min(values), 
                   max(values), 
                   int(round(mean(values))),
                   int(round(percentile(values,0.9))),
                   first, 
                   last,
                   args.target]
        else:
            out = [when, 0, 0, 0, 0, 0, 0, args.target]
        print '\t'.join(map(str,out))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
