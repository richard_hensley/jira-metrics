#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Evaluate the initiative tickets for conformance to data standards and print a report of errors

'''
import sys
import os
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.standardargs import standardmain


def addargs(parser):
    pass


def processor(args):
    ticket_count = 0
    error_count = 0
    for w in map(ticket_wrapper_factory, filter(lambda x: x.is_initiative, parse_ticket_file(args.infile[0]))):
        if not w.is_closed:
            continue
        errors = []
        ticket_count += 1
        if not w.due_date:
            errors.append('Ticket due date is missing.')
            errors.append(
                '  This can be fixed by setting the Commitment Due Date.')
            errors.append(
                '  This can also be fixed by setting the Fixed Version.')
        if not w.start_date:
            errors.append('Ticket start date is missing.')
            errors.append('  This can be fixed by setting the Start Version.')
        if errors:
            error_count += 1
            print 'https://ticket.opower.com/browse/%s' % (w.ticket.key)
            for e in errors:
                print ' ' * 4, e
    print "Ticket Count = %(ticket_count)i, Error Count = %(error_count)i" % locals()
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
