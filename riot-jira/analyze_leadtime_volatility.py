#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Diagnostic dump of Task, User Story, Technical Debt, and Bug
'''
import sys
import os
from utils.standardargs import standardmain, add_common_args
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from datetime import date
from collections import OrderedDict
from utils.stats import percentile, stddev, zscore
import logging

def addargs(parser):
    add_common_args(parser, ('-m', '-b', '-t'))
    parser.add_argument('-r', '--red', dest='red', type=float, default=1.1)
    parser.add_argument('-g', '--green', dest='green', type=float, default=0.6)

def processor(args):
    LOG = logging.getLogger()
    buckets = args.bucketer.buckets(date.today(), args.back)
    buckets = buckets[:-1]
    LOG.debug('buckets %s', str(buckets))
    report = OrderedDict()
    for b in buckets:
        report[b] = []
    LOG.debug('types %s', str(args.types))
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        LOG.debug('using file %s',fn)
        for w in filter(None, map(ticket_wrapper_factory, tickets)):
            if not w.ticket.issue_type in args.types:
                continue
            if not w.lead_time:
                continue
            bucket = args.bucketer.bucket(w.delivery_date)
            if not bucket in report:
                continue
            LOG.debug('qualifying ticket %s into bucket %s', str(w), str(bucket))
            report[bucket].append(w.lead_time)

    # calculate the 90% percentile lead time for each bucket
    for bucket in report.iterkeys():
        data = sorted(report[bucket])
        report[bucket] = percentile(data, 0.9)

    sd, m = stddev(report.values())
    print '\t'.join(map(str, ['mean', m, 'file', args.infile]))
    print '\t'.join(map(str, ['sd', sd, 'type', args.types]))
    print '\t'.join(map(str, ['red', args.red, 'mode', args.mode]))
    print '\t'.join(map(str, ['green', args.green]))
    headings = [args.mode, 'volatility', '90%_lead', 'red', 'yellow', 'green']
    print '\t'.join(headings)
    for (bucket, r) in report.iteritems():
        z = abs(zscore((r,), sd, m)[0][0])
        out = [bucket, z, r if r > 0 else '',
               z if z >= args.red else '',
               z if z < args.red and z >= args.green else '',
               z if z < args.green else '']
        print '\t'.join(map(str, out))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
