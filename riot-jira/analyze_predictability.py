#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Analyze Ticket Data and calculate predictability of a ticket as (Delivery Variance) / (Lead Time).

This program uses the following values from each ticket.
  * Start Date - When work was started on the ticket
  * Due Date - When the ticket was planned to be delivered
  * Delivery Date - When the ticket was actually delivered
  
This program makes the following calculations
  * Lead Time - (Delivery Date - Start Date)
  * Delivery Delta - (Due Date - Delivery Date)
  * Delivery Variance - (Delivery Delta / Lead Time)
  * Delivery Delta Zscore - ((Delivery Delta - mean(Delivery Delta)) / sd(Delivery Delta))
  
This program analyzes the ticket data and outputs a tab separated value file containing the predictability
values for the tickets selected from the file. The predictability is used to determine how predictable delivery
was for the tickets during each period. The variance is used to make this calculation. Any ticket with a predictability
less than the variance parameter is considered "delivered on time". The ratio of "delivered on time" to "total delivered"

This analysis program need the following parameters.

  Ticket Type(s)
  Bucketing Scheme (weekly, monthly, quarterly)
  How many buckets back
  Data File(s)

'''
import sys
import os
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from datetime import date
from collections import OrderedDict
from utils.standardargs import standardmain, add_common_args
import logging

LOG = logging.getLogger()

def calculate_current_buckets(bucketer, back):
    buckets = bucketer.buckets(date.today(), back)
    buckets = buckets[:-1]
    LOG.debug('buckets %s', str(buckets))
    result = OrderedDict()
    for b in buckets:
        if b not in result:
            result[b] = [0, 0]
    return result

def addargs(parser):
    add_common_args(parser, ('-t', '-m', '-b'))
    parser.add_argument(
        '-v', '--variance', dest='variance', type=float, required=True)
    parser.add_argument('-r', '--red', dest='red', type=float, required=True)
    parser.add_argument('-g', '--green', dest='green', type=float, required=True)

def do_filter(w, types):
    if not w or not w.due_date or not w.start_date or not w.ticket.issue_type in types:
        return False
    return True

def processor(args):  # IGNORE:C0111
    buckets = calculate_current_buckets(args.bucketer, args.back)
    LOG.debug('types %s', str(args.types))
    for fn in args.infile:
        for w in filter(lambda x: do_filter(x, args.types), map(ticket_wrapper_factory, parse_ticket_file(fn))):
            bucket = args.bucketer.bucket(w.due_date)
            if bucket in buckets:
                LOG.debug('ticket %s in bucket %s', str(w), str(bucket))
                b = buckets[bucket]
                b[0] += 1
                if w.delivery_variance and w.delivery_variance <= args.variance:
                    b[1] += 1

    print '\t'.join(map(str, ['', '', 'file', args.infile]))
    print '\t'.join(map(str, ['variance', args.variance, 'type', args.types]))
    print '\t'.join(map(str, ['red', args.red, 'mode', args.mode]))
    print '\t'.join(map(str, ['green', args.green]))
    headings = [args.mode, 'due', 'delivered',
                'predictability', 'red', 'yellow', 'green']
    print '\t'.join(headings)
    for (key, b) in buckets.iteritems():
        if b[0]:
            d = float(b[0])
            p = b[1] / d
        else:
            p = 0.0
        out = [key,
               b[0] if b[0] > 0 else '',
               b[1] if b[1] > 0 else '',
               p if p > 0 else '']
        out.append(p if p > 0. and p <= args.red else '')
        out.append(p if p > args.red and p < args.green else '')
        out.append(p if p >= args.green else '')
        print '\t'.join(map(str, out))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
