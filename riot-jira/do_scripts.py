
from subprocess import check_call
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from glob import glob
from compiler.ast import flatten
import logging
from utils.standardargs import process_logging_args

LOG = logging.getLogger()

class Call(object):
    def __init__(self, desc, verbose, logging, log_to):
        self.desc = desc
        self.out = None
        self.logging = logging
        self.log_to = log_to
        self.verbose = verbose
        self.cmd = ['python']
            
    
    def call(self):
        if len(self.cmd) == 1:
            return
        try:
            print self.desc
            fp = None
            if self.out:
                fp = open(self.out,'w')
            if self.logging:
                for c in self.cmd:
                    if c.startswith('--log='):
                        break
                else:
                    self.cmd.insert(2, '--log=%s' % (self.logging,))
            if self.log_to:
                for c in self.cmd:
                    if c.startswith('--log_to='):
                        break
                else:
                    self.cmd.insert(2, '--log_to=%s' % (self.log_to,))
            if self.verbose:
                print ' '.join(self.cmd)
            LOG.debug(' '.join(self.cmd))
            check_call(self.cmd,stdout=fp)
        finally:
            if fp:
                fp.close()

if __name__ == '__main__':
    parser = ArgumentParser(
        description='do_scripts.py', formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-j", "--jira", dest="jira", required=True)
    parser.add_argument('-p', '--project', dest='projects', action='append')
    parser.add_argument('-v', '--verbose', dest='verbose', default=False)
    parser.add_argument(dest="scripts", metavar="FILE", nargs='+')    
    parser.add_argument('-l', '--log', dest='logging', 
                        choices=('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'),
                        default='INFO')
    parser.add_argument('--log_to', dest='log_to')
    args = parser.parse_args()
    process_logging_args(args)
    parms = { 'config' : args.jira }
    try:
        files = flatten(map(glob,args.projects))
        if files:
            projects = []
            for fn in files:
                with open(fn,'r') as fp:
                    projects.extend([s.strip() for s in fp.readlines() if s.strip()])
        else:
            projects = args.projects
    except TypeError:
        projects = ['']
    scripts = flatten(map(glob,args.scripts))
    LOG.debug('projects %s',str(projects))
    LOG.debug('scripts %s',str(scripts))
    if not scripts:
        raise Exception("NO Scripts Specified %(scripts)s" % args.__dict__)      
    for fn in scripts:
        for project in projects:
            parms['project'] = project
            with open(fn) as fp:
                lines = [s.strip() for s in fp.readlines() if s.strip()]
            calls = []
            for line in lines:
                line = line % parms
                if line[0] == '#':
                    calls.append(Call(line[1:], args.verbose, args.logging, args.log_to))
                elif line[0] == '>':
                    calls[-1].out = line[1:]
                else:
                    calls[-1].cmd.append(line)
            for call in calls:
                call.call()