'''
Created on Mar 14, 2014

@author: Richard
'''
import unittest
from datetime import datetime, date
from utils.dates import date_to_str, datetime_to_str, select_datetime,\
    calc_days_between, str_to_date, str_to_datetime, select_date
from utils.utils import first

class Test(unittest.TestCase):

    def test_str_to_date(self):
        dt = str_to_date("2013-07-15T19:47:48.000-0400")
        self.assertEquals(date(2013,7,15),dt)
        
    def test_date_to_str(self):
        dt = date_to_str("2013-07-15T19:47:48.000-0400")
        self.assertEquals("07/15/2013",dt)
        
    def test_date_to_str_datetime(self):
        dt = date_to_str(datetime(2013,7,15,19,47))
        self.assertEquals("07/15/2013",dt)
        
    def test_date_to_str_date(self):
        dt = date_to_str(datetime(2013,7,15))
        self.assertEquals("07/15/2013",dt)
        
    def test_date_to_str_invalid(self):
        dt = date_to_str("ABC")
        self.assertEquals(dt,'')
 
    def test_date_to_str_none(self):
        dt = date_to_str(None)
        self.assertEquals(dt,'')

    def test_str_to_datetime(self):
        dt = str_to_datetime("2013-07-15T19:47:48.090-0400")
        self.assertEquals(datetime(2013,7,15,19,47,48,90000),dt)
        
    def test_datetime_to_str(self):
        dt = datetime_to_str("2013-07-15T19:47:48.000-0400")
        self.assertEquals("07/15/2013 19:47",dt)
        
    def test_datetime_to_str_invalid(self):
        dt = datetime_to_str("ABC")
        self.assertEquals(dt,'')
 
    def test_datetime_to_str_none(self):
        dt = datetime_to_str(None)
        self.assertEquals(dt,'')

    def test_datetime_to_str_datetime(self):
        dt = datetime_to_str(datetime(2013,7,15,19,47))
        self.assertEquals("07/15/2013 19:47",dt)
        
    def test_datetime_to_str_date(self):
        dt = datetime_to_str(datetime(2013,7,15))
        self.assertEquals("07/15/2013 00:00",dt)
        
    def test_select_datetime_min(self):
        dt = select_datetime(min,('1/2/2014',))
        self.assertEquals(datetime(2014,1,2,0,0),dt)
        
    def test_select_datetime_min_no_list(self):
        dt = select_datetime(min,'1/2/2014')
        self.assertEquals(datetime(2014,1,2,0,0),dt)
        
    def test_select_datetime_min_big_list(self):
        dt = select_datetime(min, ('1/4/2014', '1/2/2014', '', None))
        self.assertEquals(datetime(2014,1,2,0,0),dt)
    
    def test_select_datetime_min_empty_list(self):
        dt = select_datetime(min, ())
        self.assertEquals(None,dt)

    def test_select_datetime_min_invalid_list(self):
        dt = select_datetime(min, ('abc','',None))
        self.assertEquals(None,dt)
     
    def test_select_date_min(self):
        dt = select_date(min,('1/2/2014',))
        self.assertEquals(date(2014,1,2),dt)
        
    def test_select_date_min_no_list(self):
        dt = select_date(min,'1/2/2014')
        self.assertEquals(date(2014,1,2),dt)
        
    def test_select_date_min_big_list(self):
        dt = select_date(min, ('1/4/2014', '1/2/2014', '', None))
        self.assertEquals(date(2014,1,2),dt)
    
    def test_select_date_min_empty_list(self):
        dt = select_date(min, ())
        self.assertEquals(None,dt)

    def test_select_date_min_invalid_list(self):
        dt = select_date(min, ('abc','',None))
        self.assertEquals(None,dt)
        
    def test_select_date_first_list(self):
        dt = select_date(first,(date(2014,1,2), None, None, None))
        self.assertEquals(date(2014,1,2), dt)

    def test_calc_days_between(self):
        delta = calc_days_between(datetime(2014,1,13,7,30), datetime(2014,1,15,8,14))
        self.assertAlmostEquals(2.03, delta, 2)

    def test_calc_days_between1(self):
        delta = calc_days_between(datetime(2014,1,13,7,30), ((datetime(2014,1,18,8,14), None, datetime(2014,1,13,8,14))))
        self.assertAlmostEquals(5.03, delta, 2)
        
    def test_calc_days_between2(self):
        delta = calc_days_between(datetime(2014,1,13,7,30), (None, datetime(2014,1,15,8,14)))
        self.assertAlmostEquals(2.03, delta, 2)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()