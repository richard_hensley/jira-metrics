import unittest
import json
from utils import ticket
from utils.dates import select_date
from utils.utils import first
from datetime import date
import os

class Test(unittest.TestCase):

    def load_ticket(self, fn):
        try:
            with open(fn) as f:
                j = json.load(f)
        except IOError:
            with open('test/'+fn) as f:
                j = json.load(f)
        return ticket.Ticket(j) 
        
    def test_first_arrival(self):
        t = self.load_ticket('test_ticket_ds_692.json')
        d = map(t.first_arrival_date,
            ('Coding', 'Code Review', 'Ready for Testing', 'Testing', 'In Progress'))
        self.assertEquals(date(2013,8,29), select_date(first, d))
        
    def test_fix_versions(self):
        t = self.load_ticket('test_ticket_ds_1609.json')
        fv = t.fix_versions
        self.assertEquals('V4.2.x', fv[0].name)
        self.assertIsNone(fv[0].release_date)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()