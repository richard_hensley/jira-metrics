
import unittest
from utils.json_objects import JsonClass, make_list_item_factory,\
    make_value_factory
import datetime
from utils.dates import str_to_datetime, str_to_date


class Test(unittest.TestCase):

    stuff_prop_map = {
                      'str' : (None, None, 'str'),
                      'value' : (int, None, 'value'),
                      '__str__' : ('str','value')
                      }
    
    prop_map = {
           'prop1' : (None, None, 'prop1'),
           'inval1' : (None, None, 'inval'),
           'date' : (str_to_date, None, 'date'),
           'date1' : (str_to_datetime, None, 'datetime'),
           'name' : (None, None, 'dict', 'name'),
           'list' : (None, None, 'dict', 'list'),
           'list2' : (int, None, 'dict', 'list', 2),
           'stuff' : (make_list_item_factory(make_value_factory(stuff_prop_map)), None, 'stuff')
           }
    
    json = {
            'prop1' : 'value',
            'date' : '1/2/2013',
            'datetime' : "2011-10-28T15:25:27.000-0400",
            'dict' : { 
                      'name' : 'joe',
                      'list' : (4,5,6),
                      },
            'stuff' : [
                     {
                        'str' : 'abc',
                        'value' : 1
                     },
                     {
                        'str' : 'def',
                        'value' : 3
                     },
                     ]
            }


    def setUp(self):
        self.m = JsonClass(Test.json, Test.prop_map)

    def tearDown(self):
        self.m = None

    def testProp1(self):
        self.assertEquals(self.m.prop1,'value')
        
    def testDate(self):
        self.assertEquals(self.m.date,datetime.date(2013,1,2))
        
    def testDate1(self):
        self.assertEquals(self.m.date1, datetime.datetime(2011,10,28,15,25,27))
        
    def testList(self):
        self.assertEquals(len(self.m.list),3)
        
    def testList2(self):
        self.assertEquals(self.m.list2,6)
        
    def testStuffList(self):
        self.assertEquals(len(self.m.stuff),2)
        
    def testStuffStr(self):
        self.assertEquals(self.m.stuff[0].str,'abc')
        self.assertEquals(self.m.stuff[1].str,'def')
        
    def testStuffValue(self):
        self.assertEquals(self.m.stuff[0].value,1)
        self.assertEquals(self.m.stuff[1].value,3)

    def testStuffValueStr(self):
        self.assertEquals(str(self.m.stuff[0]),"abc,1")
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()