#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Print out the jira keys for all the tickets in the specified file.

This is a diagnostic utility used when trying to figure out what jira tickets are contained in a file.
'''
import sys
sys.path.insert(0,'')
import os
from utils.standardargs import standardmain
from utils.ticket import Ticket
import json
from utils.utils import parse_ticket_file


def addargs(parser):
    parser.add_argument(
        '-k', '--key', dest='keys', required=True, action='append')


def processor(args):
    for fn in args.infile:
        parse_ticket_file(fn)
        for k in args.keys:
            try:
                t = Ticket.all_tickets[k]
                print json.dumps(t._json, indent=2)
            except KeyError:
                print "Key not found %s" % (k,)

    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
