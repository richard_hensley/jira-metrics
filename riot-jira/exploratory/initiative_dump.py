#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Print out initiative tickets

'''
import sys
import os
from utils.standardargs import standardmain
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.dates import date_to_str


def addargs(parser):
    pass


def do_filter(w):
    return w and w.ticket.is_initiative


def processor(args):
    headings = ['key', 'status', 'due_date', 'start_date',
                'delivery_date', 'fix_version', 'transition']
    print '\t'.join(headings)
    for w in filter(do_filter, map(ticket_wrapper_factory, parse_ticket_file(args.infile[0]))):
        out = [w.ticket.key,
               w.ticket.status,
               date_to_str(w.due_date),
               date_to_str(w.start_date),
               date_to_str(w.delivery_date),
               w.fix_version_name,
               w.fix_version_release_date,
               '' if w.start_date else ','.join(w.ticket.transition_states),
               ]
        print '\t'.join(map(str, out))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
