#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Dump the lead time between a set of statues and a done status for the selected tickets.

This program calculates the days in status for all the tickets that have all the statuses
specified in the command line.
'''
import sys
import os
from utils.utils import parse_ticket_file
from utils.ticket import calc_status_set
from utils.standardargs import add_common_args, standardmain, CLIError


def addargs(parser):
    add_common_args(parser, ('-c', '-t'))
    parser.add_argument('-d', '--done', dest='done_column', required=True)


def processor(args):
    types = set(args.types)
    columns = args.columns
    headings = ['key', 'status', 'lead_time']
    print '\t'.join(headings)
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        statuses = calc_status_set(tickets)
        for c in columns:
            if c not in statuses:
                raise CLIError("Specified Column %s is not valid" % (c,))
        if not args.done_column in statuses:
            raise CLIError("Specified Column %s is not valid" %
                           (args.done_column,))
        for t in tickets:
            if t.status != args.done_column:
                continue
            if not t.issue_type in types:
                continue
            out = [t.key, t.status, t.lead_time(columns, args.done_column)]
            print '\t'.join([str(v) for v in out])
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
