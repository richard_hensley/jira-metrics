#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Dump the cycle time for each ticket for the statuses specified in the command line.

This program calculates the days in status for all the tickets that have all the statuses
specified in the command line.

This can be used to get a sense of where tickets are getting stuck in the system.

This is NOT a lead time analysis tool. This is a cycle time analysis tool.
'''
import sys
sys.path.insert(0,'')
import os
from utils.standardargs import add_common_args, standardmain
from utils.utils import parse_ticket_file
from utils.ticket import calc_status_set, Ticket


def addargs(parser):
    add_common_args(parser, ('-c', '-t'))


def processor(args):
    tickets = parse_ticket_file(args.infile[0])
    statuses = calc_status_set(tickets)
    types = args.types
    columns = args.columns
    for c in columns:
        if c not in statuses:
            raise Exception("Specified Column %s is not valid" % (c,))

    headings = Ticket.standard_field_header()
    headings.extend([v.lower().replace(' ', '_') for v in columns])
    print '\t'.join(headings)
    for t in tickets:
        if not t.issue_type in types:
            continue
        if t.has_all_statuses(columns):
            out = t.standard_fields
            for c in columns:
                out.append(t.days_in_status(c))
            print '\t'.join([str(v) for v in out])

    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
