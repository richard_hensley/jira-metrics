#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Diagnostic feature dump program used to investigate feature data calculations

'''
import sys
import os
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.standardargs import standardmain
from utils.ticket import Ticket
from utils.dates import date_to_str
from utils.buckets import SprintsBucketer


def addargs(parser):
    pass


def processor(args):
    headings = Ticket.standard_field_header()
    headings.extend(
        ('start_date',
         'due_date',
         'delivery_date',
         'is_planned',
         'delivered_as_planned',
         'start_version_map_date',
         'child_start_date',
         'start_status_date',
         'sprint_due'
         'links',
         'flow'))
    sprints = SprintsBucketer()

    print '\t'.join(headings)
    for fn in args.infile:
        for w in map(ticket_wrapper_factory, filter(lambda x: x.is_feature, parse_ticket_file(fn))):
            if not w.is_closed:
                continue
            out = w.ticket.standard_fields
            out.extend((date_to_str(w.start_date),
                        date_to_str(w.due_date),
                        date_to_str(w.delivery_date),
                        w.is_planned,
                        w.delivered_as_planned,
                        date_to_str(w.ticket.start_version.map_start_date),
                        date_to_str(w.child_start_date),
                        date_to_str(w.start_status_date),
                        sprints.bucket(w.due_date),
                        '' if w.start_date else ' '.join(w.child_ticket_keys),
                        '' if w.start_date else ','.join(
                            w.ticket.transition_states)
                        ))
            print '\t'.join(map(str, out))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
