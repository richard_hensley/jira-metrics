#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Diagnostic feature dump program used to investigate feature data calculations

'''
import sys
import os
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.standardargs import standardmain
from utils.dates import date_to_str
from utils.buckets import SprintsBucketer


def addargs(parser):
    pass


def processor(args):
    headings = ['project', 'key', 'status',
                'start_date',
                'due_date',
                'delivery_date',
                'fix_version_due_date',
                'planned_due_date',
                'is_planned',
                'delivered_as_planned',
                'planned_fix_version',
                'planning_anchor',
                'current_fix_version',
                'sprint_due',
                'transitions']
    print '\t'.join(headings)
    sprints = SprintsBucketer()
    for fn in args.infile:
        for w in map(ticket_wrapper_factory, filter(lambda x: x.is_feature, parse_ticket_file(fn))):
            out = [w.ticket.project, w.ticket.key, w.ticket.status,
                   date_to_str(w.start_date),
                   date_to_str(w.due_date),
                   date_to_str(w.delivery_date),
                   date_to_str(w.fix_version_due_date),
                   date_to_str(w.planned_fix_version_due_date),
                   w.is_planned,
                   w.delivered_as_planned,
                   w.planned_fix_version,
                   w.planning_anchor,
                   w.fix_version_name,
                   sprints.bucket(w.due_date),
                   ','.join(w.ticket.fix_version_transitions)
                   ]
            print '\t'.join(map(str, out))
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
