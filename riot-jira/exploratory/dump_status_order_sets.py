#!/usr/local/bin/python2.7
# encoding: utf-8
'''
This dumps the most common status sets ordered by the transition's they make.

This is used to get a sense of the status values used in the data set.
'''

import sys
import os
from utils.utils import parse_ticket_file
from utils.standardargs import standardmain, add_common_args


def inc_status(d, s):
    if s not in d:
        d[s] = 0
    d[s] += 1


def addargs(parser):
    add_common_args(parser, ('-t',))


def processor(args):
    headings = ['key', 'status', 'set']
    print '\t'.join(headings)
    for fn in args.infile:
        tickets = parse_ticket_file(fn)
        for t in tickets:
            if not t.issue_type in args.types:
                continue
            if not t.is_closed:
                continue
            if t.resolution not in ('Done', 'Fixed'):
                continue
            tr = t.transition_states
            if not tr:
                continue
            out = [t.key, t.status, ','.join(tr)]
            print '\t'.join(out)

    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
