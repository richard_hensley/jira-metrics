#!/usr/local/bin/python2.7
# encoding: utf-8
'''
This is the primary downloader script. This script is used to download and update a text file of jira
tickets in json format. The json files are used by the analysis and diagnostic scripts for their processing.

This program take the following parameters:
    Jira url
    Jira user
    Jira user password
    output file
    days back
'''
import sys
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import json
from jira.client import JIRA
from utils.standardargs import process_config_args, add_logging_args,\
    process_logging_args

def main(argv=None):  # IGNORE:C0111

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_shortdesc = 'jira_endpoint.py dumps jira data into a text file. It includes transitions, and basic data'
    try:
        # Setup argument parser
        parser = ArgumentParser(
            description=program_shortdesc,
            formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument(
            '-x', '--config', dest='configs', action='append', required=True)
        parser.add_argument(dest="endpoint", nargs='+')
        add_logging_args(parser)

        # Process arguments
        args = parser.parse_args()
        process_config_args(args)
        process_logging_args(args)
        
        options = {
            'server': args.server,
        }
        jira = JIRA(options, basic_auth=(args.user, args.password))
        print json.dumps(jira._get_json(args.endpoint[0]),indent=2)

        return 0

    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0

if __name__ == "__main__":
    sys.exit(main())
