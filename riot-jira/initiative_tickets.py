#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Print out initiative tickets

'''
import sys
import os
from datetime import date, timedelta
from utils.standardargs import standardmain, add_common_args
from utils.utils import parse_ticket_file, ticket_wrapper_factory
from utils.dates import date_to_str


def do_cmp(x, y):
    for i in (1, 2, 3, 4):
        j = cmp(x[i], y[i])
        if j == 0:
            continue
        return j
    return 0


def addargs(parser):
    add_common_args(parser, ('-b',))


def do_filter(w, when):
    if not w or not w.is_closed or not w.ticket.is_initiative or \
        not w.due_date or not w.start_date or w.due_date < when:
        return False
    return True


def processor(args):
    when = date.today() - timedelta(days=args.back)

    headings = ['key', 'status', 'due_date', 'start_date',
                'delivery_date', 'delivery_delta', 'lead_time', 'variance']
    print '\t'.join(headings)
    data = []
    for w in filter(lambda w: do_filter(w, when), map(ticket_wrapper_factory, parse_ticket_file(args.infile[0]))):
        out = [w.ticket.key,
               w.ticket.status,
               date_to_str(w.due_date),
               date_to_str(w.start_date),
               date_to_str(w.delivery_date),
               str(w.delivery_delta) if w.delivery_delta else '',
               str(w.lead_time) if w.lead_time else '',
               str(w.delivery_variance) if w.delivery_variance else '']
        data.append(
            (out,
             w.delivery_date,
             w.due_date,
             w.start_date,
             w.ticket.sortable_key))

    data.sort(key=lambda x: (x[1], x[2], x[3], x[4]))

    for d in data:
        print '\t'.join(d[0])
    return 0

if __name__ == "__main__":
    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    desc = "%s - %s" % (program_name, program_shortdesc)
    sys.exit(standardmain(addargs, processor, desc))
